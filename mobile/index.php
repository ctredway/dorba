<!DOCTYPE html> 
<?php 
	if($_SERVER['SERVER_NAME'] == "dorba.info" || $_SERVER['SERVER_NAME'] == "www.dorba.info"){
		header( 'Location: www.dorba.org'.$_SERVER['SCRIPT_NAME'] ) ;
	}
	include('detection.php');
?>
<html> 
<head> 
	<title>My Page</title> 
	<meta name="viewport" content="width=device-width, initial-scale=1"> 
	<link rel="stylesheet" href="http://code.jquery.com/mobile/1.2.0-rc.1/jquery.mobile-1.2.0-rc.1.min.css" />
	<link rel="stylesheet" href="css/mobile.css" />
	<script src="http://code.jquery.com/jquery-1.8.1.min.js"></script>
	<script src="http://code.jquery.com/mobile/1.2.0-rc.1/jquery.mobile-1.2.0-rc.1.min.js"></script>
</head> 
<body> 

<div data-role="page" id="container">

	<div data-role="header" >
		<h1><?php if($onDroid || $onIphone || $onWP){ echo 'DORBA'; } else { echo 'Dallas Off Road Bicycle Association';} ?></h1>
		<a href="#popupLogin" data-rel="popup" data-position-to="window" data-icon="gear" data-theme="e" <?php if($onDroid || $onIphone || $onWP){ echo 'data-iconpos="notext"'; } ?> class="ui-btn-right"><?php if(!$onDroid || !$onIphone || !$onWP){ echo 'Login'; } ?> </a>
		<a href="#popupLogin" data-rel="popup" data-position-to="window" data-icon="info" data-theme="b" <?php if($onDroid || $onIphone || $onWP){ echo 'data-iconpos="notext"'; } ?> class="ui-btn-left"><?php if(!$onDroid || !$onIphone || !$onWP){ echo 'Contact'; } ?></a>
		<!-- <a href="#popupLogin" data-rel="popup" data-position-to="window" data-icon="gear" class="ui-btn-right" data-role="button" data-inline="true" data-corners="true" data-shadow="true" data-iconshadow="true" data-wrapperels="span" data-theme="c" aria-haspopup="true" aria-owns="#popupLogin" ><span class="ui-btn-inner ui-btn-corner-all"><span class="ui-btn-text">Login</span></span></a> -->
	</div><!-- /header -->

	<div data-role="content">	
			<?php 
				echo "ipad: ".$onIpad."<br/>";
				echo "iph: ".$onIphone."<br/>";
				echo "droid: ".$onDroid."<br/>";			
			?>
		<div class="ui-popup-screen ui-screen-hidden" id="popupLogin-screen"></div>
		<div class="ui-popup-container ui-selectmenu-hidden" id="popupLogin-popup">
			<div data-role="popup" id="popupLogin" data-theme="a" class="ui-corner-all ui-popup ui-body-a ui-overlay-shadow" aria-disabled="false" data-disabled="false" data-shadow="true" data-corners="true" data-transition="none" data-position-to="origin">
				<form>
					<div style="padding:10px 20px;">
					  <h3>Please sign in</h3>
			          <label for="un" class="ui-hidden-accessible ui-input-text">Username:</label>
			          <input type="text" name="user" id="un" value="" placeholder="username" data-theme="a" class="ui-input-text ui-body-a ui-corner-all ui-shadow-inset">
		
			          <label for="pw" class="ui-hidden-accessible ui-input-text">Password:</label>
			          <input type="password" name="pass" id="pw" value="" placeholder="password" data-theme="a" class="ui-input-text ui-body-a ui-corner-all ui-shadow-inset">
		
			    	  <div data-corners="true" data-shadow="true" data-iconshadow="true" data-wrapperels="span" data-icon="null" data-iconpos="null" data-theme="b" class="ui-btn ui-btn-up-b ui-shadow ui-btn-corner-all ui-submit" aria-disabled="false"><span class="ui-btn-inner ui-btn-corner-all"><span class="ui-btn-text">Sign in</span></span></div>
					</div>
				</form>
			</div>
		</div>
	</div><!-- /content -->
<div data-role="footer" data-id="foo1" data-position="fixed">
	<div data-role="navbar">
		<ul>
			<li><a href="a.html">Home</a></li>
			<li><a href="b.html">Trails</a></li>
			<li><a href="c.html">Events</a></li>
			<li><a href="d.html">Sponsors</a></li>
			<li><a href="d.html">Forum</a></li>
			
		</ul>
	</div><!-- /navbar -->
</div><!-- /footer -->
</div><!-- /page -->

</body>
</html>