<?php 
	
	include("header.php");
	
	if(!isset($_REQUEST['t'])){
		header( 'Location: trails.php' ) ;
	} else {
		
		$query_catRS = "CALL GetTrails(".$_REQUEST['t'].");";
		$catRS = mysql_query($query_catRS, $conn) or die(mysql_error());
		$row_CatRS = mysql_fetch_assoc($catRS);
		$totalRows_channelRS = mysql_num_rows($catRS);
		
		$mysqli = new mysqli("127.0.0.1", "root", "", "dorba");
		$hoursSQL = "Call GetTrailWorkHours(".$_REQUEST['t'].");";
		$stewardsSQL = "Call GetTrailStewards(".$_REQUEST['t'].")";
		$query = $hoursSQL.$stewardsSQL;
		
		$results = $mysqli->multi_query($query);
		$stews = '';
		$cnt = 0;
		do {
	        /* store first result set */
	        if ($result = $mysqli->use_result()) {
	            while ($row = $result->fetch_assoc()) {
	            	if($cnt != 1){
		            	$hours = $row['totalWorked'];
	            	} else {
		            	$stews .=$row['username']." <br/>";
	            	}
	                
	            }
	            $result->close();
	            $cnt++;
	        }
	       
	    } while ($mysqli->next_result());
		
		if($totalRows_channelRS == 0){
			header( 'Location: trails.php' ) ;
		}

	}	
?>

    <div class="container-fluid">
      <div class="row-fluid">
        <div class="span9">
        	<ul class="breadcrumb">
			  <li>
			    <a href="index.php">Home</a> <span class="divider">/</span>
			  </li>
			  <li>
			    <a href="trails.php">Trails</a> <span class="divider">/</span>
			  </li>
			  <li class="active">
			    <?php echo $row_CatRS['trailName']; ?>
			  </li>
			 </ul>
        	<div class="page-header">
	        	<h2><?php echo $row_CatRS['trailName']; ?> <a id="folowMe" class="btn btn-primary btn-mini pull-right" onclick="followTrail(<?php echo $row_CatRS['trailId']; ?>)">Follow &raquo;</a> <a class="btn btn-success btn-mini pull-right" onclick="turnOnEdit();" id="editTrail" style="display:none">Edit</a></h2>
	        	<?php if($row_CatRS['currentStatus'] == 0){ ?>
		        	<div class="alert alert-error">TRAIL IS CLOSED! Please stay off the trail until the trail is opened.</div>
	        <?php	} ?>
	        </div>
        	<div class="row-fluid" id="main_content">
        		<div class="row-fluid">
	        		<div class="well well-small span4" id="trailData">
	        			<strong>Total Logged Hours:</strong> <?php echo $hours; ?> 
	        		</div>
	        		<div class="well well-small span6">
	        			<strong>Steward(s) </strong><br/> <?php  echo $stews; ?>
	        		</div>
        		</div>
	          <div class="well">
	            <?php echo $row_CatRS['trailDesc']; ?>
	          </div>
        	</div>
        	<?php include('trailedit.html'); ?>
          <div class="row-fluid">
          	
         </div><!--/row-->
        </div><!--/span-->
      
        <div class="span3">
        	
          <h4>Trail Map/Location</h4>
        	
          <div class="well sidebar-nav">
            <div id="smallMap"></div>
            <div><address><?php echo $row_CatRS['trailAddress']; ?> <a href="http://maps.google.com?q=<?php echo $row_CatRS['geoLat'].'+'.$row_CatRS['geoLang']; ?>" target="_blank">Google Map</a></address></div>
            <?php if($row_CatRS['facebook'] != '' || $row_CatRS['twitter'] != ''){ ?><h4>Social</h4>
            <div><?php if($row_CatRS['facebook'] != ''){ ?><a href="<?php echo $row_CatRS['facebook'] ?>" target="_blank"><img src="images/facebook-icon.png" class="fb" border="0"/></a><?php } if($row_CatRS['twitter'] != ''){ ?><a href="<?php echo $row_CatRS['twiiter'] ?>" target="_blank"><img src="images/twitter-icon.png" class="fb" border="0"/></a><?php } ?></div><?php } ?>
            <h4 id="photos" style="display:none">Trail Photos <a class="btn btn-success btn-mini" id="add_photo_btn" style="display:none" onclick="showPhoto()">Add Photo</a></h4>
            <form class="form_horizontal" style="display:none" id="photo_frm" method="POST" enctype="multipart/form-data">
          		<input type="hidden" name="tid" id="tid"/>
          		<input type="file" id="trail_photo" name="trail_photo">  
				<a id="cancel_edit" onclick="hide_photo_frm()" class="btn btn-danger btn-mini">Cancel</a> <a class="btn btn-success btn-mini" onclick="return ajaxFileUpload()">Save</a>
          	</form>
            <div id="trailImgs"></div>
          </div>
          <h4>Trail Updates</h4>
          <div class="well sidebar-nav">
          	<ul id="trail_cond" class="nav nav-list">
          	</ul>
          </div>
          <form class="form_horizontal" style="display:none" id="comment_frm">
          		<input type="hidden" name="t" id="t"/>
				<input type="hidden" name="by" id="u"/>
          		<textarea name="trailComment" cols="40" rows="5"></textarea>  
				<a id="cancel_edit" onclick="hide_edit_frm()" class="btn btn-danger">Cancel</a> <a class="btn btn-success pull-right" onclick="addComment()">Save</a>
          	</form>
         	<h4>Trail Comments <a class="btn btn-success btn-mini" id="add_comment_btn" style="display:none" onclick="showComment()">Add Comment</a></h4> 
           <div class="well sidebar-nav">
           		<ul id="commentList" class="nav nav-list"></ul>           
           </div>
        </div><!--/span-->
      </div><!--/row-->

      <hr>

      <footer>
        <p>&copy; DORBA 2012</p>
      </footer>

    </div><!--/.fluid-container-->
    
    <!-- Le javascript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="js/json2.js"></script>
    <script src="js/jquery-1.7.1.min.js"></script>
    <script type="text/javascript" src="admin/ckeditor/ckeditor.js"></script>
    <script type="text/javascript" src="admin/ckeditor/adapters/jquery.js"></script>
    <script type="text/javascript" src="http://maps.googleapis.com/maps/api/js?key=AIzaSyBxbb7btwaq4ADW_y5cqQlM5GV2s3pyYB0&sensor=false"></script>
    <script src="js/ajaxfileupload.js"></script>
    <script src="assets/js/bootstrap-transition.js"></script>
    <script src="assets/js/bootstrap-alert.js"></script>
    <script src="assets/js/bootstrap-modal.js"></script>
    <script src="assets/js/bootstrap-dropdown.js"></script>
    <script src="assets/js/bootstrap-scrollspy.js"></script>
    <script src="assets/js/bootstrap-tab.js"></script>
    <script src="assets/js/bootstrap-tooltip.js"></script>
    <script src="assets/js/bootstrap-popover.js"></script>
    <script src="assets/js/bootstrap-button.js"></script>
    <script src="assets/js/bootstrap-collapse.js"></script>
    <script src="assets/js/bootstrap-carousel.js"></script>
    <script src="assets/js/bootstrap-typeahead.js"></script>
    <!-- <script src="js/app.js"></script> -->
    <script src="js/login.js"></script>
    <script>
     var glat = <?php echo $row_CatRS["geoLat"]?>;
     var glong =  <?php echo $row_CatRS["geoLang"] ?>;
    </script>
    <script src="js/trail.js"></script>
  </body>
</html>
