
    <?php include("header.php") ?>

    <div class="container-fluid" id="main_content">
      <div class="row-fluid">
        
        <div class="span9">
        	<div class="alert alert-success">
        	<!-- make this more prominent --> 
			  If you are NEW or even a RETURNING member, please head to our forums <a href="/forum">here</a> and create an account.
			</div>
          <div class="hero-unit">
            <h1 class="hidden-phone">Welcome to DORBA!</h1>
            <h3 class="hidden-desktop hidden-tablet">Welcome to DORBA!</h3>
            <p>Welcome to the Dallas Off Road Bicycle Association web site.  Our primary mission is providing access to great local trails.  Your membership provides the tools and infrastructure used to build trails. It also lets local municipalities know that you care about and use the trails on their properties. Whether you are an experienced rider or someone who is just starting out, we hope you’ll find the site full useful information.
	        <br/><br/>Please browse around. A good place to start is the forum and the calendar and we encourage you join us on a ride soon. You do not have to be a DORBA Member to attend most rides/clinics.  We hope to see you on the trail soon!

            </p>
            
            <p><a href="detail.php" class="btn btn-primary btn-large">View &raquo;</a>  <a href="join.php" class="btn btn-success btn-large">Join Now &raquo;</a></p>
          </div>
          <div class="container-fluid">
	          <div class="row-fluid">
	          		<div id="eventArea" class="span9">
	          		</div>
		          <div id="dorbaStats" class="span3">
		          	<?php include('member.php'); ?>
		          </div>
	          </div>
          </div>
           
        </div><!--/span-->
        <div class="span2 hidden-phone">
          <div class="well sidebar-nav">
            <ul class="nav nav-list">
              <li class="nav-header">Trail Listing</li>
              <?php 				
              	date_default_timezone_set('America/Chicago');
				$query_catRS = "CALL GetTrails(0);";
				$catRS = mysql_query($query_catRS, $conn) or die(mysql_error());
				$row_CatRS = mysql_fetch_assoc($catRS);
				//$totalRows_channelRS = mysql_num_rows($catRS);

              	$open = "class='open'";
              	$closed = "class='closed'";
              	$status='';
              do { 
	              if($row_CatRS['currentStatus'] == 1){ $statusClass=$open; $status = "Open";} else { $statusClass=$closed; $status="Closed";};
	              $d = date("m-d-Y");
	              if($row_CatRS['updateFormatted'] != null){
		              $d = $row_CatRS['updateFormatted'];
	              } 
	              $content = "Conditions: ".$row_CatRS['conditionDesc']."<br/>Status: ".$status." as of ".$d;
              ?> 
              <li><a <?php echo $statusClass; ?> href="trail.php?t=<?php echo $row_CatRS['trailId']; ?>" rel="popover" data-content="<?php echo $content; ?>" data-original-title="<?php echo $row_CatRS['trailName']; ?>"><?php echo $row_CatRS['trailName']; ?></a></li>
              <?php } while ($row_CatRS = mysql_fetch_assoc($catRS)); mysql_free_result($catRS); 
	             
              ?>       
            </ul>
            <ul class="nav nav-list" id="sponsorList">
              <li class="nav-header">DORBA Sponsors</li>
                   
            </ul>
          </div><!--/.well -->
          
        </div><!--/span-->
        
      </div><!--/row-->

      <hr>

      <footer>
        <p>&copy; DORBA 2012</p>
              </footer>

    </div><!--/.fluid-container-->
    <div class="modal hide" id="phoneLogin_frm">
    	<div id="mhead" class="modal-header">Login</div>
    	<div class="modal-body">
	    	<form id="login_frm2">
			  <input type="text" name="u" placeholder="User Name" class="input-small"/> <input type="password" name="p" placeholder="Password" class="input-small"/>
			  
			</form>
		</div>
		<div class="modal-footer"><a id="sign_in2" class="btn btn-primary btn-mini">Login</a></div>
    </div>
        <!-- Le javascript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="js/json2.js"></script>
    <script src="assets/js/jquery.js"></script>
    <script src="assets/js/bootstrap-transition.js"></script>
    <script src="assets/js/bootstrap-alert.js"></script>
    <script src="assets/js/bootstrap-modal.js"></script>
    <script src="assets/js/bootstrap-dropdown.js"></script>
    <script src="assets/js/bootstrap-scrollspy.js"></script>
    <script src="assets/js/bootstrap-tab.js"></script>
    <script src="assets/js/bootstrap-tooltip.js"></script>
    <script src="assets/js/bootstrap-popover.js"></script>
    <script src="assets/js/bootstrap-button.js"></script>
    <script src="assets/js/bootstrap-collapse.js"></script>
    <script src="assets/js/bootstrap-carousel.js"></script>
    <script src="assets/js/bootstrap-typeahead.js"></script>
    <script src="js/login.js"></script>
    <script src="js/app.js"></script>
    
  </body>
</html>
