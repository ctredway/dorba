<?php include("../Connections/conn.php"); ?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <title>DORBA Frozen Series Results</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">

    <!-- Le styles -->
    
    <link href="assets/css/bootstrap.css" rel="stylesheet"> 
    <link href="site.css" rel="stylesheet">
    <style>
      body {
        padding-top: 60px; /* 60px to make the container go all the way to the bottom of the topbar */
      }
    </style>
    <link href="assets/css/bootstrap-responsive.css" rel="stylesheet">

    <!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
      <script src="//html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->

<body>

<div class="navbar navbar-fixed-top">
      <div class="navbar-inner">
        <div class="container-fluid">
          <a class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse">
            <span class="i-bar"></span>
            <span class="i-bar"></span>
            <span class="i-bar"></span>
          </a>
          <a class="brand" href="#">DORBA Race Results</a>
          <div class="nav-collapse">
            <ul class="nav">
              <li id="cgmnu" class="active"><a href="index.php">2011 Fall Series</a></li>
              <li id="revmnu"><a href="frozen.php">2012 Frozen Series</a></li>
               <li id="revmnu"><a href="fall2012.php">2012 Fall Series</a></li>
            </ul>
			
          </div><!--/.nav-collapse -->
        </div>
      </div>
    </div>


<table width="100%" border="0" cellpadding="0" cellspacing="0">
	<tr>
		<td align="center" width="750">
		<img src="header.png" alt="header" width="800" height="175" />
	</tr>
	<tr>
		<td align="center" class="nav" width="750">Show Me Only: <a onclick="hideShow('pro');">Pro</a> | <a onclick="hideShow('cat1');">Cat 1</a> | <a onclick="hideShow('cat2');">Cat 2</a> | <a onclick="hideShow('cat3');">Cat 3</a> | <a onclick="hideShow('all');">All</a></td>
	</tr>
	<tr>
		<td align="center" width="750">
<?php 
	$query_catRS = "SELECT typeId,typeValue,displayOrder from types where typeName = 'cat' and active = 1 and series = 1 order by displayOrder";
	$catRS = mysql_query($query_catRS, $conn) or die(mysql_error());
	$row_CatRS = mysql_fetch_assoc($catRS);
	$totalRows_channelRS = mysql_num_rows($catRS);
	
	do{
		if($row_CatRS['typeValue'] == "XC Cat 1"){
			$cat = "cat1";
		} else if($row_CatRS['typeValue'] == "XC Cat 2"){
			$cat = "cat2";
		} else if($row_CatRS['typeValue'] == "XC Cat 3"){
			$cat = "cat3";
		} else {
			$cat = "pro";
		}

 ?>
 <div id="<?php echo $cat; ?>">
 <table width="750" border="0" cellpadding="2" cellspacing="1">
 	<tr>
 		<td class="cat"><?php echo $row_CatRS['typeValue']; ?></td>
 	</tr>
 </table>

<?php 
	$query_racerRS = "SELECT typeId,typeValue from types where typeName = 'Age Group' and active = 1";
	$channelRS = mysql_query($query_racerRS, $conn) or die(mysql_error());
	$row_RS = mysql_fetch_assoc($channelRS);
	$totalRows_RS = mysql_num_rows($channelRS);
	
	if($totalRows_RS > 0){
	?>
	
	<table width="750" border="0" cellpadding="1" cellspacing="1">
	
	<?php
	
	
	do {
?> 
	
<?php
		//echo $row_RS['racerId']."<br>";
		$query_resultRS = "SELECT racerId,name,team,total from racers inner join tally on racerId = racer where ageGroup = ".$row_RS['typeId']." and class = ".$row_CatRS['typeId']." and racers.series = 1 order by total desc";
		$resultsRS = mysql_query($query_resultRS, $conn) or die(mysql_error());
		$resultRow_RS = mysql_fetch_assoc($resultsRS);
		$totalRacers = mysql_num_rows($resultsRS);
		
		if($totalRacers !=0){
?>
	<tr>
		<td class="agegroup" colspan="3"><?php echo $row_RS['typeValue']; ?></td>
	</tr>
	<tr class="header">
		<td>Name</td>
		<td>Team</td>
		<td>&nbsp;</td>
	</tr>
<?php
		$rowClass = 'rowLight';
		do{
			//$place ++;
		?>
			<tr class="<?php echo $rowClass; ?>">
				<td onclick="showResults(<?php echo $resultRow_RS['racerId']; ?>)" style="cursor:pointer"><?php echo $resultRow_RS['name']; ?></td>
				<td><?php echo $resultRow_RS['team']; ?></td>
				<td class="points" align="center"><?php echo $resultRow_RS['total']; ?></td>
			</tr>
		<?php
			if($rowClass == 'rowLight'){
				$rowClass = 'rowDark';
			} else {
				$rowClass = 'rowLight';
			}
			
			$q_racerRes = "select * from results inner join races on raceId = race where racer = ".$resultRow_RS['racerId'];
			$racerResResultS = mysql_query($q_racerRes, $conn) or die(mysql_error());
			$racerResRow_RS = mysql_fetch_assoc($racerResResultS);
			
		?>
			<tr>
				<td colspan="3">
					<div id="<?php echo $racerResRow_RS['racer']; ?>" style="display:none">
					<table width="100%" border="0" cellpadding="1" cellspacing="1">
					<tr class="raceHeader">
						<td>Race</td>
						<td>Place</td>
						<td>Lap 1 Time</td>
						<td>Lap 1 Pace</td>
						<td>Lap 2 Time</td>
						<td>Lap 2 Pace</td>
						<td>Lap 3 Time</td>
						<td>Lap 3 Pace</td>
					</tr>
		<?php
			$resultRowClass = 'raceLight';
			do{
		?>
					<tr class="<?php echo $resultRowClass; ?>">
						<td><?php echo $racerResRow_RS['raceName']; ?></td>
						<td><?php echo $racerResRow_RS['place']; ?></td>
						<td><?php echo $racerResRow_RS['lap1Time']; ?></td>
						<td><?php echo $racerResRow_RS['lap1Pace']; ?></td>
						<td><?php echo $racerResRow_RS['lap2Time']; ?></td>
						<td><?php echo $racerResRow_RS['lap2Pace']; ?></td>
						<td><?php echo $racerResRow_RS['lap3Time']; ?></td>
						<td><?php echo $racerResRow_RS['lap3Pace']; ?></td>
					</tr>	
		<?php 
			if($resultRowClass == 'raceLight'){
				$resultRowClass = 'raceDark';
			} else {
				$resultRowClass = 'raceLight';
			}
			
			} while ($racerResRow_RS = mysql_fetch_assoc($racerResResultS)); ?>
						</table>
					</div>
				</td>
			</tr>
		<?php
			
			} while ($resultRow_RS = mysql_fetch_assoc($resultsRS));
				$place = 0;
			}
		} while ($row_RS = mysql_fetch_assoc($channelRS));
?>
</table>
</div>
<?php
		}
	} while ($row_CatRS = mysql_fetch_assoc($catRS));  
?>

</td>
	</tr>
</table>

<script src="assets/js/jquery.js"></script>
<script src="assets/js/bootstrap-scrollspy.js"></script>

<script>
	function hideShow(what){
		//alert(what);
		switch(what){
			case "cat1":
				$("#cat1").show();
				$("#cat2").hide();
				$("#cat3").hide();
				$("#pro").hide();
				break;
			case "cat2":
				$("#cat1").hide();
				$("#cat2").show();
				$("#cat3").hide();
				$("#pro").hide();
				break;
			case "cat3":
				$("#cat1").hide();
				$("#cat2").hide();
				$("#cat3").show();
				$("#pro").hide();
				break;
			case "pro":
				$("#cat1").hide();
				$("#cat2").hide();
				$("#cat3").hide();
				$("#pro").show();
				break;
			default:
				$("#cat1").show();
				$("#cat2").show();
				$("#cat3").show();
				$("#pro").show();
				break;
		}
		
	}
	
	function showResults(what){
		$("#"+what).toggle();
	}
</script>
</body>
</html>