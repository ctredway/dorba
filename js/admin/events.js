$(function(){
	//start up code
	$("#addEvent").click(function(){
		showNewEvent();
	});
	
	$("#saveEvent").click(function(){
		publishEvent();
	});
	
	
});
btn = '<button type="button" class="close pull-right" data-dismiss="modal" aria-hidden="true" id="closeMe" onclick="closeMe()">×</button>';

function codeAddress() {
	//console.log('code');
    geocoder = new google.maps.Geocoder();
    address = $("#eventLocation").val();
    geocoder.geocode( { 'address': address}, function(results, status) {
      if (status == google.maps.GeocoderStatus.OK) {
      	rslt = results[0].geometry.location;
      	//console.log(results[0].geometry.location);
        
        $("#geoLat").val(rslt['Xa']);
        $("#geoLang").val(rslt['Ya']);
        
      } else {
        alert("Geocode was not successful for the following reason: " + status);
      }
    });
    
}

function deleteEvent(event){
	$.get('../services/deleteevent.php?d='+event,function(data){
		window.location = 'events.php';
	});
}

function closeMe(){
	$("#newEventFrm").fadeToggle("fast", "linear");
	$("#sidebar").fadeToggle("slow", "linear");
	$("#middle").fadeToggle("slow", "linear");
}

function showNewEvent(){
	$("#hdr").html('New Event '+btn);
	clearForm();
	$(".jquery_ckeditor").ckeditor();
	$("#newEventFrm").fadeToggle("fast", "linear");
	$("#sidebar").fadeToggle("slow", "linear");
	$("#middle").fadeToggle("slow", "linear");
	buildDatePickers();
}

function clearForm(){
	$("#eventId").val('');
	$("#eventName").val('');
	$("#eventStart").val('');
	$("#eventEnd").val('');
	$("#editor1").val('');
	$("#shortDesc").val('');
	$("#homepage").val(0);
}

function editEvent(event){
	
	clearForm();
	//$( "#eventEnd" ).datepicker();
	$.getJSON('../services/events.php?e='+event,function(data){
		//console.log(data.events[0].event.eventId);
		var e = data.events[0].event;
		$("#hdr").html(e.eventName+ ' ' +btn);
		$("#eventId").val(e.eventId);
		$("#eventName").val(e.eventName);
		$("#eventStart").val(e.eventStart);
		$("#eventEnd").val(e.eventEnd);
		$("#shortDesc").val(e.eventShortDesc);
		$("#editor1").val(e.eventDesc);
		//console.log(e.showOnHomePage);
		
		$(".jquery_ckeditor").ckeditor();
		$("#newEventFrm").fadeToggle("fast", "linear");
		$("#sidebar").fadeToggle("slow", "linear");
		$("#middle").fadeToggle("slow", "linear");
		buildDatePickers();
		$("#homepage").val(e.showOnHomePage);
	});
}

function showImages(event){
	$("#eventfiles_modal").modal('show');
	$.getJSON('../services/eventfiles.php?e='+event,function(data){
		
	});
}

function addEventFile(){
	var fileToUse = "";
	
	//if(which == "top"){
		fileToUse = "services/uploadtrailimage.php";	
	//} 
	
	//alert(which+'_slideimage ' + fileToUse);

	$.ajaxFileUpload
	(
		{
			url:fileToUse,
			secureuri:false,
			fileElementId:'trail_photo',
			dataType: 'json',
			data:{trail:tt, id:'id'},
			success: function (data, status)
			{
				if(typeof(data.error) != 'undefined')
				{
					if(data.error != '')
					{
						alert("ERROR: " + data.error);
					}else
					{
						
						//if(which == "top"){
							//alert(data.msg);
							//$('#trailImgs').empty();
							$('#trailImgs').append("<img src='images/trails/"+data.msg+"'/>");
							image = "<img src='images/trails/"+data.msg+"'/>";
							//alert($('#topContent').val());
						//} 
					}
				}
			},
			error: function (data, status, e)
			{
				alert(e);
			}
		}
	)
	
	return false;
}

function closeFiles(){
	$("#eventfiles_modal").modal('hide');
}

function publishEvent(){
	
	$.post('../services/saveevent.php',$("#newevent_frm").serialize(),function(data){
		window.location='events.php';
		//console.log(data);
	});
	
}

function buildDatePickers(){
	$( "#eventStart" ).datepicker({
		onSelect: function(data,instance){
			$( "#eventStart" ).datepicker( "option", "dateFormat", 'yy-mm-dd' );
		}
	});
	
	$( "#eventEnd" ).datepicker({
		onSelect: function(data,instance){
			$( "#eventEnd" ).datepicker( "option", "dateFormat", 'yy-mm-dd' );
		}
	});
}