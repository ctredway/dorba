$(function(){
	$(".jquery_ckeditor").ckeditor();
	$("#addSponsor").click(function(){
		newSponsor();
	});
	
	$("#saveSponsorBtn").click(function(){
		saveSponsor();
	});
	
	$("#closeMe").click(function(){
		hideMe('sponsorForm');
	});
	grabSponsors();
});

var close = '<button type="button" class="close pull-right" aria-hidden="true" id="closeMe" onclick="hideMe()">×</button>';

function editSponsor(sponsor){
	$.getJSON("../services/sponsors.php?s="+sponsor,function(data){
		
		newSponsor();
		var s = data.sponsors[0].sponsor;
		$("#hdr").html('Edit '+s.sponsorName+close);
		$("#sid").val(s.sponsorId);
		$("#sponsorName").val(s.sponsorName);
		$("#website").val(s.sponsorUrl);
		$("#contact").val(s.contact);
		$("#editor1").val(s.sponsorDesc);
		//$("#sponsorForm").show();
		
	});
}

function saveSponsor(){
	
	$.post('../services/savesponsor.php',$("#sponsor_frm").serialize(),function(data){
		grabSponsors();
		newSponsor();
	});
}

function newSponsor(){
	$("#hdr").html('New Sponsor'+close);
	$("#sid").val('0');
	$("#sponsorName").val('');
	$("#website").val('');
	$("#editor1").val('');
	$("#contact").val('');
	if( $('#sponsorForm').is(':visible') ) {
		
		//$("#sponsorForm").hide();
	} else {

    	// it's not visible so do something else
    	$("#sponsorForm").show();
    }
}

function hideMe(what){
	if(!what){
		what = 'sponsorForm';
	}
	$("#"+what).hide();
}

function grabSponsors(){
	$.getJSON('../services/sponsors.php',function(data){
		$("#sponsorlistBody").empty();
		$.each(data.sponsors, function(i,item){
			$("#sponsorlistBody").append('<tr><td><a href="#" onclick="editSponsor('+item.sponsor.sponsorId+')"; class="btn btn-success btn-mini"><i class="icon-edit icon-white"/></a> <a href="#" class="btn btn-primary btn-mini"><i class="icon-map-marker icon-white"/></a></td><td><a href="../sponsor.php?s='+item.sponsor.sponsorId+'">'+item.sponsor.sponsorName+'</a></td><td>'+item.sponsor.sponsorUrl+'</td></tr>');
		});
	});
}