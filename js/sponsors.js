$(function(){
	
	$("#showWeather").click(function(){
		toggleWeather();
	});
	
	$("#sign_in").click(function(){
	   doLogin();
   });
	
	if(readCookie("d_user") != null){
   		u = readCookie("d_user");
   		//console.log("WHAT THE: " + u);
   		if( /Android|webOS|iPhone|iPod|BlackBerry/i.test(navigator.userAgent) ) {
			buildPhoneUserPnl(JSON.parse(u)); 
		} else {
			buildUserPnl(JSON.parse(u));
		}
	   
   } else {
	   //console.log("asdasdsad");
   }
});

var cgMap=[];
function mapInit(what) {
	//console.log('init');
	geocoder = new google.maps.Geocoder();
	
	var mapOptions = {zoom: 10,mapTypeId: google.maps.MapTypeId.ROADMAP,zoomControl:true,zoomControlOptions: {
            style: google.maps.ZoomControlStyle.SMALL
          },mapTypeControl: true,
          mapTypeControlOptions: {
            style: google.maps.MapTypeControlStyle.DROPDOWN_MENU
          } };
	cgMap = new google.maps.Map(document.getElementById('spmap'),mapOptions);
	
	codeAddress(what);
}

function codeAddress(address) {
	//console.log('code');
    geocoder = new google.maps.Geocoder();
    
    geocoder.geocode( { 'address': address}, function(results, status) {
      if (status == google.maps.GeocoderStatus.OK) {
      	//console.log(results[0].geometry.location);
        cgMap.setCenter(results[0].geometry.location);
        
        var cgmarker = new google.maps.Marker({
            map: cgMap, 
            position: results[0].geometry.location
        });
      } else {
        alert("Geocode was not successful for the following reason: " + status);
      }
    });
    //grabTrails();
    grabSponsors();
}

function addPin(location,name){
	geocoder = new google.maps.Geocoder();
    
    geocoder.geocode( { 'address': location}, function(results, status) {
      if (status == google.maps.GeocoderStatus.OK) {
      	
        
         var image = new google.maps.MarkerImage("images/sponsorPin.png",
            new google.maps.Size(20, 25),
            new google.maps.Point(0,0),
            new google.maps.Point(10, 25));
            
          //clickable area
          var shape = {
	      	coord: [1, 1, 1, 20, 18, 20, 18 , 1],
	        type: 'poly'
	      };
        
        var cgmarker = new google.maps.Marker({
            map: cgMap, 
            icon:image,
            position: results[0].geometry.location,
            shape:shape,
            title:name,
        });
        
      } else {
        //alert("Geocode was not successful for the following reason: " + status);
      }
    });

}

show_weather = false;
weatherLayer = new google.maps.weather.WeatherLayer({temperatureUnits: google.maps.weather.TemperatureUnit.FAHRENHEIT });
	cloudLayer = new google.maps.weather.CloudLayer();
function toggleWeather(){
	
	if(show_weather == false){
		$("#showWeather").html("Hide Weather");
		weatherLayer.setMap(cgMap);
		cloudLayer.setMap(cgMap);
        
        show_weather = true;
	} else {
		$("#showWeather").html("Show Weather");
		show_weather = false;
		weatherLayer.setMap(null);
		cloudLayer.setMap(null);
		
	}
		
}

function focuspin(lat,lang){
	//console.log("lat: "+lat);
	
	var newLatLang = new google.maps.LatLng(lat,lang);
	cgMap.setCenter(newLatLang);
	cgMap.setZoom(15);
	
}

function unfocuspin(lat,lang,open){
	//console.log("pin unfocus");
	
}

function grabSponsors(){
	var rr = new Date().getTime();
	$.getJSON('services/sponsors.php?'+rr,function(data){
		$.each(data.sponsors, function(i,item){
			$("#sponsorList_tbl").append('<tr><td><!-- <i class="icon-map-marker makehand"></i> --> <a href="sponsor.php?s='+item.sponsor.sponsorId+'">'+item.sponsor.sponsorName+'</a></td><td><div id="'+item.sponsor.sponsorId+'"></div></td></tr>');
			$.getJSON('services/sponsorlocations.php?s='+item.sponsor.sponsorId,function(data){
				$.each(data.locations,function(i,item){
					addy = item.location.address+ ' ' + item.location.city;
					title = item.location.sponsorName +  ' - ' + item.location.city;
					
					$("#"+item.location.sponsorId).append(addy+"<br/>");
					addPin(addy,title);
					
				});
			});
			
		});
	});
	
}

function showSponsor(sponsor){
	var rr = new Date().getTime();
	$.getJSON('services/sponsorlocations.php?s='+sponsor+'&'+rr,function(data){
		$.each(data.locations,function(i,item){
			
			
		});
	});
}

function resetMap(){
	geocoder = new google.maps.Geocoder();
    
	    geocoder.geocode( { 'address': "Dallas, Texas"}, function(results, status) {
	      if (status == google.maps.GeocoderStatus.OK) {
	      	//console.log(results[0].geometry.location);
	        cgMap.setCenter(results[0].geometry.location);
	        cgMap.setZoom(9);
	      }
	
		});
}

mapInit("Dallas, Texas");
