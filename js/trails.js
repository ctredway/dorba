$(function(){
	var steward = readCookie("d_steward");
	//console.log(steward.toString());
	$("#viewAllTrails").click(function(){
		resetMap();
	});
	
	$("#viewAllTrails2").click(function(){
		resetMap();
	});
	
	$("#showWeather").click(function(){
		toggleWeather();
	});
	
	$("#sign_in").click(function(){
	   doLogin();
   });

	if(readCookie("d_user") != null){
   		u = readCookie("d_user");
   		//console.log("WHAT THE: " + u);
   		if( /Android|webOS|iPhone|iPod|BlackBerry/i.test(navigator.userAgent) ) {
			buildPhoneUserPnl(JSON.parse(u)); 
		} else {
			buildUserPnl(JSON.parse(u));
		}
	   
    } else {
	   //console.log("asdasdsad");
    }
   
   mapInit("Dallas, Texas");
});

var cgMap=[];
function mapInit(what) {
	//console.log('init');
	geocoder = new google.maps.Geocoder();
	
	var mapOptions = {zoom: 8,mapTypeId: google.maps.MapTypeId.ROADMAP,zoomControl:true,zoomControlOptions: {
            style: google.maps.ZoomControlStyle.SMALL
          },mapTypeControl: true,
          mapTypeControlOptions: {
            style: google.maps.MapTypeControlStyle.DROPDOWN_MENU
          } };
	cgMap = new google.maps.Map(document.getElementById('cgmap'),mapOptions);
	
	codeAddress(what);
}

function codeAddress(address) {
	//console.log('code');
    geocoder = new google.maps.Geocoder();
    
    geocoder.geocode( { 'address': address}, function(results, status) {
      if (status == google.maps.GeocoderStatus.OK) {
      	//console.log(results[0].geometry.location);
        cgMap.setCenter(results[0].geometry.location);
        
        var cgmarker = new google.maps.Marker({
            map: cgMap, 
            position: results[0].geometry.location
        });
      } else {
        alert("Geocode was not successful for the following reason: " + status);
      }
    });
    grabTrails();
    grabSponsors();
}

function addPin(location,name){
	geocoder = new google.maps.Geocoder();
    
    geocoder.geocode( { 'address': location}, function(results, status) {
      if (status == google.maps.GeocoderStatus.OK) {
      	
        
         var image = new google.maps.MarkerImage("images/sponsorPin.png",
            new google.maps.Size(20, 25),
            new google.maps.Point(0,0),
            new google.maps.Point(10, 25));
            
          //clickable area
          var shape = {
	      	coord: [1, 1, 1, 20, 18, 20, 18 , 1],
	        type: 'poly'
	      };
        
        var cgmarker = new google.maps.Marker({
            map: cgMap, 
            icon:image,
            position: results[0].geometry.location,
            shape:shape,
            title:name
        });
      } else {
        //alert("Geocode was not successful for the following reason: " + status);
      }
    });

}

show_weather = false;
weatherLayer = new google.maps.weather.WeatherLayer({temperatureUnits: google.maps.weather.TemperatureUnit.FAHRENHEIT });
cloudLayer = new google.maps.weather.CloudLayer();
function toggleWeather(){
	
	if(show_weather == false){
		$("#showWeather").html("Hide Weather");
		weatherLayer.setMap(cgMap);
		cloudLayer.setMap(cgMap);
        
        show_weather = true;
	} else {
		$("#showWeather").html("Show Weather");
		show_weather = false;
		weatherLayer.setMap(null);
		cloudLayer.setMap(null);
		
	}
		
}

function grabTrails(){
	//console.log('trails');
	var rr = new Date().getTime();
	$.getJSON('services/trails.php?'+rr,function(data){
		//console.log(data.trails);
		$("#openlistBody").empty();
		$("#closedlistBody").empty();
		$("#closedTrail").hide();
		$.each(data.trails, function(i,item){
			//console.log(i);
			
             var statusClass = 'open';
             var status = 1;
             var where = "openlistBody";
		   	  if(item.trail.currentStatus != 1){
		   	  	statusClass = 'closed';
		   	  	status=0;
		   	  	where = "closedlistBody";
		   	  	$("#closedTrail").show();
		   	  }
		   	  var dispLocation='';
		   	  if(item.trail.trailAddress == ""){dispLocation = item.trail.trailCity} else {dispLocation = item.trail.trailAddress}
		   	  buttonClass = "btn-primary";
		   	  buttonTxt = "Follow";
		   	  if(item.trail.trailId == 22 || item.trail.trailId == 2){buttonClass = 'btn-success';buttonTxt = "Unfollow";}
		   	  
		   	  var editBtn = '';
		   	  
		   	  if(readCookie("d_steward1") && readCookie("d_user")){
		   	  	//console.log(readCookie("d_steward1"));
		   	  		s = JSON.parse(readCookie("d_steward1"));
		   	  		//console.log(s);
		   	  		$.each(s.trails, function(i,item2){
			   	  		if(item2.trail.trail == item.trail.trailId){
				   	  		editBtn = '<a onclick="editTrail('+item.trail.trailId+');" class="btn btn-success btn-mini"><i class="icon-comment"/></a> <!-- <a onclick="fulledit('+item.trail.trailId+');" class="btn btn-warning btn-mini"><i class="icon-pencil"/><a/> -->';
				   	  	}
				   	});
		   	  }
		   	  var new_date = item.trail.updateFormatted;
		   	  if(item.trail.updateFormatted == null){
		   	  	new_date = '';
			   	 
		   	  }
		   	  //console.log('date: ' + new_date);
			$('#'+where).append('<tr><td width="70">'+editBtn+'</td><td><i class="icon-map-marker makehand" onclick="focuspin('+item.trail.geoLat+','+item.trail.geoLang+')"/> <a href="trail.php?t='+item.trail.trailId+'" class="'+statusClass+'">'+item.trail.trailName+'</a></td><td>'+dispLocation+'</td><td>'+item.trail.conditionDesc+'<br/><p class="text-success"><small>'+new_date+'</small></p></td></tr>');
			
			   if(item.trail.geoLat != "0"){
			   	  
			      var newLatLang = new google.maps.LatLng(item.trail.geoLat,item.trail.geoLang);
			      var statusImg='images/openPin.png';
			      if(item.trail.currentStatus != 1){
				      statusImg = 'images/closedPin.png';
			      }
			      //build map pin based on status
			      var image = new google.maps.MarkerImage(statusImg,
		            new google.maps.Size(20, 25),
		            new google.maps.Point(0,0),
		            new google.maps.Point(10, 25));
		            
		          //clickable area
		          var shape = {
			      	coord: [1, 1, 1, 20, 18, 20, 18 , 1],
			        type: 'poly'
			      };
			      //console.log(newLatLang);
				  var marker = new google.maps.Marker({
		            map: cgMap, 
		            position: newLatLang,
		            icon:image,
		            shape:shape,
		            title:item.trail.trailName,
		            id:item.trail.trailId
		          });
		          
		          google.maps.event.addListener(marker, 'click', function() {
			          //map.setZoom(8);
			          //map.setCenter(marker.getPosition());
			          window.location = 'trail.php?t='+marker.id;
			      });
		       }
		       
		      
		    });
		    
			
		});
	
}

function replaceDiv( div )
{
	var config = {
		toolbar:
		[
			['Bold', 'Italic', '-', 'NumberedList', 'BulletedList', '-', 'Link', 'Unlink'],
			['UIColor']
		]
	};
	
	$(".jquery_ckeditor").ckeditor(config);
	
}

function fulledit(trail){
	$("#trailFrm").modal();
	replaceDiv(  );
	var rr = new Date().getTime();
	$.getJSON('services/conditions.php?'+rr,function(data){
		$("#conditions").empty();
		$.each(data.conditions, function(i,item){
			$("#conditions").append('<option value="'+item.condition.conditionId+'">'+item.condition.conditionDesc+'</option>');
		});
	});
	
	$.getJSON('services/trails.php?t='+trail,function(data){
		//alert(data.trails[0].trail.trailName);
		$("#trailName").val("");
		$("#trailAddy").val("");
		$("#trailCity").val("");
		$("#landOwner").val("");
		$("#facebook").val("");
		$("#twitter").val("");
		$(".jquery_ckeditor").val("");
		$("#trailId").val("");
		var trail = data.trails[0].trail;
		//replaceDiv("trail_Desc");
		$("#trailName").val(trail.trailName);
		$("#trailAddy").val(trail.trailAddress);
		$("#trailCity").val(trail.trailCity);
		$("#landOwner").val(trail.landOwner);
		$("#facebook").val(trail.facebook);
		$("#twitter").val(trail.twitter);
		$("#editor1").val(trail.trailDesc);
		$("#trailId").val(trail.trailId);
		
	});
	$("#trailFrm").modal('show');
}

function save_edit_frm(){
	
	var rr = new Date().getTime();
	$.post('services/updatetrailstatus.php?'+rr,$("#frm_edit_trail").serialize(),function(data){
		$("#edit_frm_div").modal('hide')
		grabTrails();
	});
}

function editTrail(trail){
	//alert(trail);
	//$("#trailList_tbl").hide();
	//$("#edit_frm_div").show();
	$("#edit_frm_div").modal();
	
		
	$("#t").val(trail);
	var us = JSON.parse(readCookie("d_user"));
	$("#u").val(us.user_id);
	var rr = new Date().getTime();
	$.getJSON('services/conditions.php?'+rr,function(data){
		$("#condition").empty();
		$.each(data.conditions, function(i,item){
			$("#condition").append('<option value="'+item.condition.conditionId+'">'+item.condition.conditionDesc+'</option>');
		});
	});
	
	$("#edit_frm_div").modal('show');
}

function hide_edit_frm(){
	//alert(trail);
	$("#t").val(0);
	$("#trailList_tbl").show();
	$("#edit_frm_div").hide();
}

function focuspin(lat,lang){
	//console.log("lat: "+lat);
	
	var newLatLang = new google.maps.LatLng(lat,lang);
	cgMap.setCenter(newLatLang);
	cgMap.setZoom(15);
	
}

function unfocuspin(lat,lang,open){
	//console.log("pin unfocus");
	
}

function grabSponsors(){
	var rr = new Date().getTime();
	$.getJSON('services/sponsors.php?'+rr,function(data){
		$.each(data.sponsors, function(i,item){
			$("#sponsorList").append('<li><!-- <i class="icon-map-marker makehand" onclick="showSponsor('+item.sponsor.sponsorId+');"></i> --><a href="sponsor.php?s='+item.sponsor.sponsorId+'">'+item.sponsor.sponsorName+'</a></li>');
		});
	});
	
}

function showSponsor(sponsor){
	var rr = new Date().getTime();
	$.getJSON('services/sponsorlocations.php?s='+sponsor+'&'+rr,function(data){
		$.each(data.locations,function(i,item){
			addy = item.location.address+ ' ' + item.location.city;
			title = item.location.sponsor;
			addPin(addy,title);
		});
	});
}

function resetMap(){
	geocoder = new google.maps.Geocoder();
    
    geocoder.geocode( { 'address': "Dallas, Texas"}, function(results, status) {
      if (status == google.maps.GeocoderStatus.OK) {
      	//console.log(results[0].geometry.location);
        cgMap.setCenter(results[0].geometry.location);
        cgMap.setZoom(9);
      }

	});
}