function logout(){
	//alert("erasing");
	eraseCookie("d_user");
	window.location = window.URL;
}

function doLogin(){
	var rr = new Date().getTime();
	$.post('services/login.php?'+ rr,$("#login_frm").serialize(),function(data){
		//alert("!"+data+"!");
		if(data != "0" && data != "" && data != null && data != 0){
			u = JSON.parse(data);
			createCookie("d_user",data,2);
			buildUserPnl(u);
			//console.log(data);
			readCookie("d_user");
		} else {
			alert('Your login is incorrect, please try again');
		}
		
	});
}

function doPhoneLogin(){
	var rr = new Date().getTime();
	$.post('services/login.php?'+ rr,$("#login_frm2").serialize(),function(data){
		//alert("!"+data+"!");
		if(data != "0" && data != "" && data != null && data != 0){
			u = JSON.parse(data);
			createCookie("d_user",data,2);
			buildPhoneUserPnl(u);
			//console.log(data);
			readCookie("d_user");
		} else {
			alert('Your login is incorrect, please try again');
		}
		 
	});
}

function buildUserPnl(u){
	//console.log(u);
	//u = JSON.parse(usr);
	$("#show_user_name").empty();
	$("#show_user_name").html("Welcome, "+u.username);
	$("#login_div").hide();
	$("#usr_prof").append('<a href="profile.php?u='+u.user_id+'">My Profile</a>');
	$("#profile_mnu").show();
	var rr = new Date().getTime();
	$.post('services/checkfortrails.php?t='+u.user_id+'&'+rr,function(data){
		//console.log(JSON.parse(data));
		if(data != 0){
			eraseCookie("d_steward1");
			eraseCookie("d_steward");
			createCookie("d_steward1",data,8);	
			//console.log(readCookie("d_steward1"));
		}
		
	});
}

function buildPhoneUserPnl(u){

	//console.log(u);
	//u = JSON.parse(usr);
	$("#show_user_name").empty();
	$("#show_user_name").html(u.username);
	$("#usr_prof").append('<a href="profile.php?u='+u.user_id+'">My Profile</a>');
	$("#profile_mnu").show();
	var rr = new Date().getTime();
	$.post('services/checkfortrails.php?t='+u.user_id+'&'+rr,function(data){
		//console.log(JSON.parse(data));
		if(data != 0){
			eraseCookie("d_steward1");
			eraseCookie("d_steward");
			createCookie("d_steward1",data,8);	
		}
		
	});
	$("#showPhoneLogin_btn").hide();
}

//cookie functions
function createCookie(name,value,days) {
    if (days) {
        var date = new Date();
        date.setTime(date.getTime()+(days*24*60*60*1000));
        var expires = "; expires="+date.toGMTString();
    }
    else var expires = "";
    document.cookie = name+"="+value+expires+"; path=/";
}

function readCookie(name) {
    var nameEQ = name + "=";
    var ca = document.cookie.split(';');
    
    for(var i=0;i < ca.length;i++) {
        var c = ca[i];
        while (c.charAt(0)==' ') c = c.substring(1,c.length);
        //console.log(c);
        if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length,c.length);
    }
    return null;
}

function eraseCookie(name) {
    createCookie(name,"",-1);
}