$(function(){
	var opt = {"placement":"left"};
	$("a[rel=popover]").popover(opt).click(function(e) {
        //e.preventDefault()
        //alert('what');
      })
    grabSponsors();
    getHomePageEvents();
      
   $("#home").addClass("active");
   $("#trails").removeClass("active"); 
   $("#forums").removeClass("active");
   $("#events").removeClass("active");
   $("#sponsors").removeClass("active");
   
   $("#sign_in").click(function(){
	   doLogin();
   });
   
   $("#sign_in2").click(function(){
   	   $("#phoneLogin_frm").modal('hide');
	   doPhoneLogin();
   });
   
   $("#showPhoneLogin_btn").click(function(){
	   showPhoneLogin();
   });
   
   if(readCookie("d_user") != null){
   		u = readCookie("d_user");
   		//console.log("WHAT THE: " + u);
   		if( /Android|webOS|iPhone|iPod|BlackBerry/i.test(navigator.userAgent) ) {
			buildPhoneUserPnl(JSON.parse(u)); 
			$("#phoneLogin").hide();
		} else {
			buildUserPnl(JSON.parse(u));
		}
	   
   } else {
	   //console.log("asdasdsad");
   }
    
});

function showPhoneLogin(){
	$("#phoneLogin_frm").modal();
	$("#phoneLogin_frm").modal('show');
}

function showQuickTrailInfo(trail){
	
}

function grabSponsors(){
	var rr = new Date().getTime();
	$.getJSON('services/sponsors.php?'+ rr,function(data){
		$.each(data.sponsors, function(i,item){
			$("#sponsorList").append('<li><a class="sponsor" href="sponsor.php?s='+item.sponsor.sponsorId+'">'+item.sponsor.sponsorName+'</a></li>');
		});
	});
	
}

function getHomePageEvents(){
	var rr = new Date().getTime();
	$.getJSON('services/homepageevents.php?'+rr,function(data){ 
		var events = data.events;
		buildEvents(events); 
	});
}

function buildEvents(evtList){
		
	begin = '<div class="row-fluid">';
	end = "</div>";
	html = "";
	//evt = [];
	for(i=0;i<evtList.length;i++){
		evt = evtList[i].event;
		//console.log(event);
		if(i==0){
			html += begin;
		}
		
		html += '<div class="span4"><h4>'+evt.eventName+'</h4>'+evt.eventShortDesc+'<p><a class="btn" href="event.php?e='+evt.eventId+'">View event &raquo;</a></p></div>';
		if(i % evtList.length == 2){
			html += end+begin;	
		}
	}
	html += end;
	//console.log(html);
	$("#eventArea").append(html);
}