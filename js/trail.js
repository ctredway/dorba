$(function(){
	$("#home").removeClass("active");
	$("#trails").addClass("active"); 
	$("#forums").removeClass("active");
	$("#events").removeClass("active");
	$("#sponsors").removeClass("active");
	
	$("#sign_in").click(function(){
	   doLogin();
	 });
	 
	 $("#saveTrail").click(function(){
		//console.log(editor.getData());
		$.post("services/savetrail.php", $("#trail_frm").serialize(),
	    	function(data){
	        	window.location = 'trail.php?t='+tt;
		  	}
		);
	});

	if(readCookie("d_user") != null){
   		u = readCookie("d_user");
   		//console.log("WHAT THE: " + u);
   		if( /Android|webOS|iPhone|iPod|BlackBerry/i.test(navigator.userAgent) ) {
   			s = readCookie("d_steward1");
			u = readCookie("d_user");
			//console.log("WHAT THE: " + u);
			stew = JSON.parse(s); 
			me = JSON.parse(u);
		
			if(stew.trail == getUrlVars()['t']){
				$("#editTrail").show();
				$("#photos").show();
				$("#add_photo_btn").show();
			}
			$("#add_comment_btn").show();
			$("#u").val(me.username);
			buildPhoneUserPnl(JSON.parse(u)); 
		} else {
			s = readCookie("d_steward1");
			u = readCookie("d_user");
			//console.log("WHAT THE: " + u);
			stew = JSON.parse(s);
			me = JSON.parse(u);
			
			$.each(stew.trails, function(i,item2){
				//console.log(item2.trail);
				if(item2.trail.trail == getUrlVars()['t']){
					$("#editTrail").show();
					$("#photos").show();
					$("#add_photo_btn").show();
				}
			});
			
			$("#add_comment_btn").show();
			$("#u").val(me.username);
			buildUserPnl(JSON.parse(u));
		}
	   
   } else {
	   //console.log("asdasdsad");
	    $("#add_comment_btn").hide();
   }
	var tt = getUrlVars()['t'];
	getComments(tt);
	getConditions(tt);
	getWatchers(tt);
	grabPhotos(tt);
	$("#t").val(tt);
	$("#tid").val(tt);
});

tt = getUrlVars()['t'];

function showPhoto(){
	$("#photo_frm").show();
}

function hide_photo_frm(){
	$("#photo_frm").hide();
}

function getUrlVars() {
    var vars = {};
    var parts = window.location.href.replace(/[?&]+([^=&]+)=([^&]*)/gi, function(m,key,value) {
        vars[key] = value;
    });
    return vars;
}

function closeEdit(){
	
	$("#main_content").show();
	$("#trailFrm").hide('fast');
}

function turnOnEdit(){
	var rr = new Date().getTime();
	$.getJSON('services/conditions.php?'+rr,function(data){
		$("#conditions").empty();
		$.each(data.conditions, function(i,item){
			$("#conditions").append('<option value="'+item.condition.conditionId+'">'+item.condition.conditionDesc+'</option>');
		});
	});
	
	$.getJSON('services/trails.php?t='+tt,function(data){
		
		$("#trailName").val("");
		$("#trailAddy").val("");
		$("#trailCity").val("");
		$("#landOwner").val("");
		$("#facebook").val("");
		$("#twitter").val("");
		$(".jquery_ckeditor").val("");
		$("#trailId").val("");
		var trail = data.trails[0].trail;
		replaceDiv("trail_Desc");
		$("#trailName").val(trail.trailName);
		$("#trailAddy").val(trail.trailAddress);
		$("#trailCity").val(trail.trailCity);
		$("#landOwner").val(trail.landOwner);
		$("#facebook").val(trail.facebook);
		$("#twitter").val(trail.twitter);
		$("#editor1").val(trail.trailDesc);
		$("#trailId").val(trail.trailId);
		$("#trailStatus").val(trail.currentStatus);
		$("#conditions").val(trail.currentCondition);
	});

	$("#trailFrm").show();
	$("#main_content").hide('fast');
}

function replaceDiv( div )
{
	var config = {
		toolbar:
		[
			['Bold', 'Italic', '-', 'NumberedList', 'BulletedList', '-', 'Link', 'Unlink'],
			['UIColor']
		]
	};
	
	$(".jquery_ckeditor").ckeditor(config);
}

function getWatchers(trail){
	var rr = new Date().getTime();
	$.getJSON('services/trailwatchers.php?t='+trail+'&'+rr,function(data){
		if(data.watchers.length > 0){
			$.each(data.watchers, function(i,item){
				var u = JSON.parse(readCookie("d_user"));
				if(u.user_id == item.watcher.member){
					$("#folowMe").removeClass('btn-primary');
					$("#folowMe").addClass('btn-danger');
					$("#folowMe").empty();
					$("#folowMe").html('Unfollow');
				}
			});
		} else {
			
		}	
	});
}

var trailMap=[];
function mapInit(what) {
	//console.log('init');
	geocoder = new google.maps.Geocoder();
	
	var myOptions = {zoom: 15,mapTypeId: google.maps.MapTypeId.ROADMAP };
	trailMap = new google.maps.Map(document.getElementById('smallMap'),myOptions);
	
	codeAddress();
}

function codeAddress() {
	//console.log('code');
   // geocoder = new google.maps.Geocoder();
  
   //console.log(geo);
    var newLatLang = new google.maps.LatLng(glat, glong);
      	//console.log(newLatLang);
    trailMap.setCenter(newLatLang);
    
    var cgmarker = new google.maps.Marker({
        map: trailMap, 
        position: newLatLang
    });
    //grabTrails();
}

function grabPhotos(trail){
	$.getJSON('services/trailphotos.php?t='+trail,function(data){
		$.each(data.photos,function(i,item){
			$("#trailImgs").append("<img src='images/trails/"+item.photo.photo+"'/>");
		})
		
	});
}

function showComment(){
	$("#comment_frm").show();
}

function hide_edit_frm(){
	$("#comment_frm").hide('fast');
}

function followTrail(trail,follow){
	
	u = readCookie("d_user");
	m = JSON.parse(u);
	if(m.user_id != null){
		//console.log(m.user_id);
		$.get('services/followtrail.php?t='+trail+'&u='+m.user_id,function(data){
			window.location = 'trail.php?t='+trail;
		});
	} else {
		alert('Please Login to Follow a trail');
	}
}

function getConditions(trail){
	var rr = new Date().getTime();
	$.getJSON('services/trailconditions.php?t='+trail+'&'+rr,function(data){
		if(data.conditions.length > 0){
			$.each(data.conditions, function(i,item){
				var cmmt = "";
				if(item.condition.trailComment != ""){
					cmmt = "<br/>- "+item.condition.trailComment;
				}
				 
				$("#trail_cond").append('<li><strong>'+item.condition.conditionDesc+' on '+ item.condition.updateFormatted +'</strong>'+cmmt+'</li>');
			});
		} else {
			$("#trail_cond").append('<li>No Current Updates</li>');
		}	
	});
}

function getComments(trail){
	//console.log(trail);
	$.getJSON('services/trailcomments.php?t='+trail,function(data){
		$("#commentList").empty();
		$.each(data.comments, function(i,item){
			$("#commentList").append('<li><strong>'+item.comment.leftBy+'</strong> - '+item.comment.updateFormatted+'<br/>'+item.comment.trailComment+'</li>');
		});
	});
}

function addComment(){
	$.post('services/addtrailcomment.php',$("#comment_frm").serialize(),function(data){
		getComments(tt);
		hide_edit_frm();
	});
}

function ajaxFileUpload(){
	/*
$("#loading")
	.ajaxStart(function(){
		$(this).show();
	})
	.ajaxComplete(function(){
		$(this).hide();
	});
*/
	
	var fileToUse = "";
	
	//if(which == "top"){
		fileToUse = "services/uploadtrailimage.php";	
	//} 
	
	//alert(which+'_slideimage ' + fileToUse);

	$.ajaxFileUpload
	(
		{
			url:fileToUse,
			secureuri:false,
			fileElementId:'trail_photo',
			dataType: 'json',
			data:{trail:tt, id:'id'},
			success: function (data, status)
			{
				if(typeof(data.error) != 'undefined')
				{
					if(data.error != '')
					{
						alert("ERROR: " + data.error);
					}else
					{
						
						//if(which == "top"){
							//alert(data.msg);
							//$('#trailImgs').empty();
							$('#trailImgs').append("<img src='images/trails/"+data.msg+"'/>");
							image = "<img src='images/trails/"+data.msg+"'/>";
							//alert($('#topContent').val());
						//} 
					}
				}
			},
			error: function (data, status, e)
			{
				alert(e);
			}
		}
	)
	
	return false;

}
mapInit();