$(function(){
	var uu;
	if(readCookie("d_user") != null){
		u = readCookie("d_user");
		//console.log("WHAT THE: " + u);
		uu = JSON.parse(u);
		$("#memberId").val(uu.user_id);
		//console.log($("#memberId").val());
		buildUserPnl(JSON.parse(u));
		getTrailWork(uu.user_id);
	} else {
		//console.log("asdasdsad");
		
	}
	
	if(readCookie("d_user") != null){
   		u = readCookie("d_user");
   		uu = JSON.parse(u);
   		$("#memberId").val(uu.user_id);
   		//console.log("WHAT THE: " + u);
   		if( /Android|webOS|iPhone|iPod|BlackBerry/i.test(navigator.userAgent) ) {	
			buildPhoneUserPnl(JSON.parse(u)); 
			getTrailWork(uu.user_id);
		} else {
			buildUserPnl(JSON.parse(u));
			getTrailWork(uu.user_id);
		}
	   
   } else {
	   //console.log("asdasdsad");
	   $("#choose").hide();
		$("#workArea").hide();
		$("#loggedOut").show();
   }
	
	$("#workId").val("0");
	
	 $("#sign_in").click(function(){
	   doLogin();
	 });
	 
	 if(readCookie("d_user") != null){
   		u = readCookie("d_user");
   		//console.log("WHAT THE: " + u);
	    buildUserPnl(JSON.parse(u));
	  } else {
		   //console.log("asdasdsad");
	  }
	 
	 $("#saveWork").click(function(){
	 	//console.log($("#work_frm").serialize());
	 	if($("#workId").val() ==0){
		 	$.post('services/savetrailwork.php',$("#work_frm").serialize(),function(data){
				//console.log(uu.user_id);
				$("#workId").val(0);
				getTrailWork(uu.user_id);
			});
	 	} else {
		 	$.post('services/updatetrailwork.php',$("#work_frm").serialize(),function(data){
				//console.log(uu.user_id);
				$("#workId").val(0);
				getTrailWork(uu.user_id);
			});
	 	}
		 
	 });
	
	$( "#workDate" ).datepicker({
		onSelect: function(data,instance){
			$( "#workDate" ).datepicker( "option", "dateFormat", 'mm-dd-yy' );
		}
	});
	
	$("#newWork").click(function(){
		$("#hours").val(''); 
		$("#work").val('');
		$("#workDate").val('');
		$("#workId").val("0");
	});
});

function getTrailWork(member){
	$("#trailWorkBody").empty();
	$("#summary").empty();
	$("#hours").val(''); 
	$("#work").val('');
	$("#workDate").val(''); 
	//$("#workId").val('0');
	//$("#memberId").val('0');
	var hours = 0;
	$.getJSON('services/gettrailwork.php?m='+member,function(data){
		$.each(data.works, function(i,item){
			$("#trailWorkBody").append('<tr><td><a class="btn btn-success btn-mini" onclick="editWork('+item.work.trailWorkId+')"><i class="icon-edit"/></a> <a class="btn btn-danger btn-mini" onclick="deleteWork('+item.work.trailWorkId+','+member+')"><i class="icon-remove"/></a></td><td>'+item.work.trail+'</td><td>'+item.work.comments+'</td><td>'+item.work.hours+' on '+item.work.workDate+'</td></tr>');
			hours = hours+parseFloat(item.work.hours);
		});
		$("#trailWorkBody").append('<tr class="success"><td colspan="3"></td><td colspan="4" id="summary">Total hours worked: ' + hours+'</td></tr>');
	});
}

function editWork(work){
	$("#workId").val("");
	 $.getJSON('services/getwork.php?w='+work,function(data){
	 	//console.log(data.works);
		$("#hours").val(data.works[0].work.hours); 
		$("#work").val(data.works[0].work.comments);
		$("#workDate").val(data.works[0].work.workDate); 
		$("#workId").val(data.works[0].work.trailWorkId);
		$("#memberId").val(data.works[0].work.member);
		
		$("#trail").val(data.works[0].work.trail);
	 });
}

function deleteWork(work,member){
	console.log(member);
	$.get('services/deletework.php?w='+work,function(data){
		getTrailWork(member);
	});
}

function logout(){
	//alert("erasing");
	eraseCookie("d_user");
	window.location = window.URL;
}

function doLogin(){
	var rr = new Date().getTime();
	$.post('services/login.php?'+ rr,$("#login_frm").serialize(),function(data){
		//alert("!"+data+"!");
		if(data != "0" && data != "" && data != null && data != 0){
			u = JSON.parse(data);
			createCookie("d_user",data,2);
			buildUserPnl(u);
			//console.log(data);
			readCookie("d_user");
			$("#choose").show();
			$("#workArea").show();
			$("#loggedOut").hide();
		} else {
			alert('Your login is incorrect, please try again');
		}
		
	});
}

function buildUserPnl(u){
	//console.log(u);
	//u = JSON.parse(usr);
	$("#show_user_name").empty();
	$("#show_user_name").html("Welcome, "+u.username);
	$("#login_div").hide();
	$("#profile_mnu").show();
	var rr = new Date().getTime();
	$.post('services/checkfortrails.php?t='+u.user_id+'&'+rr,function(data){
		//console.log(JSON.parse(data));
		if(data != 0){
			eraseCookie("d_steward1");
			eraseCookie("d_steward");
			createCookie("d_steward1",data,8);	
		}
		
	});
}

//cookie functions
function createCookie(name,value,days) {
    if (days) {
        var date = new Date();
        date.setTime(date.getTime()+(days*24*60*60*1000));
        var expires = "; expires="+date.toGMTString();
    }
    else var expires = "";
    document.cookie = name+"="+value+expires+"; path=/";
}

function readCookie(name) {
    var nameEQ = name + "=";
    var ca = document.cookie.split(';');
    
    for(var i=0;i < ca.length;i++) {
        var c = ca[i];
        while (c.charAt(0)==' ') c = c.substring(1,c.length);
        //console.log(c);
        if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length,c.length);
    }
    return null;
}

function eraseCookie(name) {
    createCookie(name,"",-1);
}