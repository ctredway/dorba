<?php include("header.php") ?>
<?php 
	//include("Connections/conn.php"); 
	
	$query_catRS = "CALL GetTrails(0);";
	$catRS = mysql_query($query_catRS, $conn) or die(mysql_error());
	$row_CatRS = mysql_fetch_assoc($catRS);
	$totalRows_channelRS = mysql_num_rows($catRS);
	
?>
    <div class="container-fluid">
    <ul class="breadcrumb">
	  <li>
	    <a href="index2.php">Home</a> <span class="divider">/</span>
	  </li>
	  <li class="active">
	    <a >Events</a> 
	  </li>
	</ul>
	<div class="page-header">
    	<h1>DORBA Events</h1>
    </div>
      <div class="row-fluid">
        
        <div class="span8">
          <ul id="myTab" class="nav nav-tabs">
              <li class="active"><a href="#calendar" data-toggle="tab">Calendar</a></li>
              <li class=""><a href="#elisting" data-toggle="tab">Event List</a></li>    
              <li class="" style="display:none"><a href="#evtMgr" data-toggle="tab">Event Manager</a></li>
          </ul>
          <div id="eventContent" class="tab-content">
              <div class="tab-pane fade active in" id="calendar">
                 <div id="calendar"></div>
              </div>
              <div class="tab-pane fade" id="elisting">
                
              </div>
              <div id="evtMgr" class="tab-pane fade"></div>
          </div>
        </div><!--/span-->
        <div class="span2 offset1">
          <div class="well sidebar-nav">
            <ul class="nav nav-list">
              <li class="nav-header">Trail Listing</li>
              <?php 
              	$open = "class='open'";
              	$closed = "class='closed'";
              	$status='';
              do { 
	              if($row_CatRS['currentStatus'] == 1){ $statusClass=$open; $status = "Open";} else { $statusClass=$closed; $status="Closed";};
	              $content = "Conditions: ".$row_CatRS['conditionDesc']."<br/>Status: ".$status;
              ?> 
              <li><a <?php echo $statusClass; ?> href="trail.php?t=<?php echo $row_CatRS['trailId']; ?>" rel="popover" data-content="<?php echo $content; ?>" data-original-title="<?php echo $row_CatRS['trailName']; ?>"><?php echo $row_CatRS['trailName']; ?></a></li>
              <?php } while ($row_CatRS = mysql_fetch_assoc($catRS));  ?>       
            </ul>
            <ul class="nav nav-list" id="sponsorList">
              <li class="nav-header">DORBA Sponsors</li>
                   
            </ul>
          </div><!--/.well -->
          
        </div><!--/span-->
        
      </div><!--/row-->

      <hr>

      <footer>
        <p>&copy; DORBA 2012</p>
      </footer>

    </div><!--/.fluid-container-->
    <div class="modal hide fade" id="eventModal">
	    <div class="modal-header">
	    	<span id="eventName"></span>
	    	<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
	    </div>
	    <div class="modal-body" id="eventBody"></div>
	    
    </div>
    
    <!-- Le javascript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="js/json2.js"></script>
    <script src="assets/js/jquery.js"></script>
    <script type='text/javascript' src='jquery/jquery-1.8.0.min.js'></script>
    <script type='text/javascript' src='jquery/jquery-ui-1.8.23.custom.min.js'></script>
    <script type='text/javascript' src='fullcalendar/fullcalendar.js'></script>
    <script src="assets/js/bootstrap-transition.js"></script>
    <script src="assets/js/bootstrap-alert.js"></script>
    <script src="assets/js/bootstrap-modal.js"></script>
    <script src="assets/js/bootstrap-dropdown.js"></script>
    <script src="assets/js/bootstrap-scrollspy.js"></script>
    <script src="assets/js/bootstrap-tab.js"></script>
    <script src="assets/js/bootstrap-tooltip.js"></script>
    <script src="assets/js/bootstrap-popover.js"></script>
    <script src="assets/js/bootstrap-button.js"></script>
    <script src="assets/js/bootstrap-collapse.js"></script>
    <script src="assets/js/bootstrap-carousel.js"></script>
    <script src="assets/js/bootstrap-typeahead.js"></script>
    <script src="js/login.js"></script>
    <script src="js/events.js"></script>
  </body>
</html>
