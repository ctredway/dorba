<?php include("Connections/conn.php"); ?>
<?php 
	if($_SERVER['SERVER_NAME'] == "dorba.info" || $_SERVER['SERVER_NAME'] == "www.dorba.info"){
		header( 'Location: http://www.dorba.org'.$_SERVER['SCRIPT_NAME'] ) ;
	}
	
	include_once('mobile/detection.php');
?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <title>Dallas Off Road Bicycle Association</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
    <meta http-equiv="cache-control" content="max-age=0" />
	<meta http-equiv="cache-control" content="no-cache" />
	<meta http-equiv="expires" content="0" />
	<meta http-equiv="expires" content="Tue, 01 Jan 1980 1:00:00 GMT" />
	<meta http-equiv="pragma" content="no-cache" />
    <!-- Le styles -->
    <link href="assets/css/bootstrap.css" rel="stylesheet">
    <link href="assets/css/bootstrap-responsive.css" rel="stylesheet">
    <link href="site.css" rel="stylesheet">
    <style type="text/css">
      body {
       <?php if($onIE){ ?>
	    padding-top: 60px;
        padding-bottom: 40px;
        <?php } ?>
      }
      .sidebar-nav {
        padding: 9px 0;
      }
      
    </style>
    <link rel="stylesheet" href="assets/css/ui-lightness/jqui.css" />
    <link rel='stylesheet' type='text/css' href='fullcalendar/fullcalendar.css' />
    <link rel='stylesheet' type='text/css' href='fullcalendar/fullcalendar.print.css' media='print' />
    <!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
      <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->

    <!-- Le fav and touch icons -->
    <link rel="shortcut icon" href="assets/ico/favicon.ico">
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="assets/ico/apple-touch-icon-144-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="assets/ico/apple-touch-icon-114-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="assets/ico/apple-touch-icon-72-precomposed.png">
    <link rel="apple-touch-icon-precomposed" href="assets/ico/apple-touch-icon-57-precomposed.png">
  </head>

  <body>

<div class="navbar navbar-fixed-top navbar-inverse">
      <div class="navbar-inner">
        <div class="container-fluid">
          <a class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse">
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </a>
          <a class="brand" href="/"><img src="images/fredBadge.png"/></a>
          
	       <div class="btn-group pull-right" id="profile_mnu" style="display:none">
            <a class="btn dropdown-toggle" data-toggle="dropdown" href="#">
              <i class="icon-user"></i> <span id="show_user_name">Login</span>
              <span class="caret"></span>
            </a>
            <ul class="dropdown-menu">
            	<li id="usr_prof"></li>
              <li><a href="trailwork.php" class="disabled">Trail Work</a></li>
              <li class="divider"></li>
              <li><a href="#" id="sign_out" onclick="logout();">Sign Out</a></li>
            </ul>
          </div>	
			<div class="nav-inner pull-right hidden-phone" id="login_div">
				<form class="navbar-form pull-right" id="login_frm">
			  <input type="text" name="u" placeholder="User Name" class="input-small"/> <input type="password" name="p" placeholder="Password" class="input-small"/>
			  <a id="sign_in" class="btn btn-primary btn-mini">Login</a>
			</form>
			</div>
			<div class="nav-inner pull-right hidden-desktop hidden-tablet" id="phone_Login">
				<a id="showPhoneLogin_btn" class="btn btn-primary btn-mini">Login</a>
			</div>
          <div class="nav-collapse">
            <ul class="nav">
              <li id="home"><a href="index.php">Home</a></li>
              <li id="trails" class="dropdown">
              	<a href="#" class="dropdown-toggle" data-toggle="dropdown">Trails <b class="caret"></b></a>
              	<ul class="dropdown-menu">
				  <li><a href="trails.php">Trail List</a></li>
				  <li><a href="trailhours.php">Trail Hours</a></li>
				</ul>
              </li>
              <li id="events"><a href="events.php">Events</a></li>
              <li id="forums"><a href="/forum">Forum</a></li>
              <li id="sponsors"><a href="sponsors.php">Sponsors</a></li>
              <li id="contact"><a href="contact.php">Contact</a></li>
              <li id="join"><a href="join.php">Join</a></li>
            </ul>
          </div>
        </div>
      </div>
    </div>
<!--
<li class="dropdown">
<a href="#" class="dropdown-toggle" data-toggle="dropdown">Dropdown <b class="caret"></b></a>
<ul class="dropdown-menu">
  <li><a href="#">Action</a></li>
  <li><a href="#">Another action</a></li>
  <li><a href="#">Something else here</a></li>
  <li class="divider"></li>
  <li><a href="#">Separated link</a></li>
</ul>
</li>
-->