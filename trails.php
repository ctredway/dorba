
    <?php include("header.php") ?>

    <div class="container-fluid">
    
      <div class="row-fluid"> 
        
        <div class="span9">
        	<ul class="breadcrumb">
			  <li>
			    <a href="/">Home</a> <span class="divider">/</span>
			  </li>
			  <li class="active">
			    <a >Trails</a> 
			  </li>
			  
			</ul>
        	<div class="page-header">
	        	<h1>DORBA Trail System</h1>
	        </div>
        	<div class="row-fluid">
        		<div><a id="showWeather" class="btn btn-success btn-small">Show Weather</a></div>
	          <div class="well" id="cgmap">
	            
	          </div>
        	</div>
          <div class="row-fluid">
          
          <div class="span6" id="openSpan">
          <h4>Open</h4>
          <table class="table table-striped table-bordered table-condensed" id="trailList_tbl">
			<thead>
			  <tr>
			    <th><a class="btn btn-info btn-mini" id="viewAllTrails">View All</a></th>
			    <th>Name</th>
			    <th>Location</th>
			    <th>Condition</th>
			  </tr>
			</thead>
			<tbody id="openlistBody"></tbody>
		</table>
			<div id="edit_frm_div" class="modal hide fade">
				
				<div class="modal-header"><strong>Update Trail</strong>
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
				</div>
				<div class="modal-body">
					<form id="frm_edit_trail" class="form-horizontal">
						<input type="hidden" name="t" id="t"/>
						<input type="hidden" name="u" id="u"/>
						<div class="control-group">
							<label class="control-label" for="trailStatus">Trail Status:</label>
							<div class="controls">
								<select name="trailStatus"><option value="0">Closed</option><option value="1">Open</option></select>
							</div>
						</div>
						<div class="control-group">
							<label class="control-label" for="condition">Trail Condition:</label>
							<div class="controls">
								<select name="condition" id="condition"></select>
							</div>
						</div>
						<div class="control-group">
							
							<div class="controls">
								<textarea name="trailComment" cols="40" rows="5"></textarea>  <span class="help-inline">Leave a comment</span>
							</div>
						</div>
						
					</form>
				</div>
				<div class="modal-footer">
					<a id="cancel_edit" data-dismiss="modal" class="btn btn-danger">Cancel</a> <a class="btn btn-success pull-right" onclick="save_edit_frm()">Submit</a>
				</div>
			</div>
          </div>
            <div class="span6" style="display:none" id="closedTrail">
           <h4>Closed</h4>
	            <table class="table table-striped table-bordered table-condensed" id="trailList_tbl">
			<thead>
			  <tr>
			    <th><a class="btn btn-info btn-mini" id="viewAllTrails2">View All</a></th>
			    <th>Name</th>
			    <th>Location</th>
			    <th>Condition</th>
			  </tr>
			</thead>
			<tbody id="closedlistBody"></tbody>
		</table>
            </div>
          </div><!--/row-->
        </div><!--/span-->
        
        <div class="span2">
          <div class="well sidebar-nav">
            <ul class="nav nav-list" id="sponsorList">
              <li class="nav-header">DORBA Sponsors</li>
                   
            </ul>
          </div>
        </div><!--/span-->
      </div><!--/row-->

      <hr>

      <footer>
        <p>&copy; DORBA 2012</p>
      </footer>

    </div><!--/.fluid-container-->
    
    <?php 
		include('trailedit.html');
    ?>
    
    <!-- Le javascript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="js/json2.js"></script>
    <script src="assets/js/jquery.js"></script>
    <script type="text/javascript" src="admin/ckeditor/ckeditor.js"></script>
    <script type="text/javascript" src="admin/ckeditor/adapters/jquery.js"></script>
    <script src="assets/js/bootstrap-transition.js"></script>
    <script src="assets/js/bootstrap-alert.js"></script>
    <script src="assets/js/bootstrap-modal.js"></script>
    <script src="assets/js/bootstrap-dropdown.js"></script>
    <script src="assets/js/bootstrap-scrollspy.js"></script>
    <script src="assets/js/bootstrap-tab.js"></script>
    <script src="assets/js/bootstrap-tooltip.js"></script>
    <script src="assets/js/bootstrap-popover.js"></script>
    <script src="assets/js/bootstrap-button.js"></script>
  
    <script src="assets/js/bootstrap-collapse.js"></script>
  <!--    <script src="assets/js/bootstrap-carousel.js"></script>
    <script src="assets/js/bootstrap-typeahead.js"></script>
-->
    <script type="text/javascript" src="http://maps.googleapis.com/maps/api/js?key=AIzaSyBxbb7btwaq4ADW_y5cqQlM5GV2s3pyYB0&sensor=false&libraries=weather"></script>
    <!-- <script src="js/app.js"></script> -->
    <script src="js/login.js"></script>
    <script src="js/trails.js"></script>
    
    <script>
    $(function(){
	   $("#home").removeClass("active");
	   $("#trails").addClass("active"); 
	   $("#forums").removeClass("active");
	   $("#events").removeClass("active");
	   $("#sponsors").removeClass("active");
    });
    </script>
  </body>
</html>
