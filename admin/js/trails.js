var cgMap=[];
function mapInit(what) {
	//console.log('init');
	geocoder = new google.maps.Geocoder();
	
	var mapOptions = {zoom: 8,mapTypeId: google.maps.MapTypeId.ROADMAP,zoomControl:true,zoomControlOptions: {
            style: google.maps.ZoomControlStyle.SMALL
          },mapTypeControl: true,
          mapTypeControlOptions: {
            style: google.maps.MapTypeControlStyle.DROPDOWN_MENU
          } };
	cgMap = new google.maps.Map(document.getElementById('cgmap'),mapOptions);
	
	codeAddress(what);
}

function codeAddress(address) {
	//console.log('code');
    geocoder = new google.maps.Geocoder();
    
    geocoder.geocode( { 'address': address}, function(results, status) {
      if (status == google.maps.GeocoderStatus.OK) {
      	//console.log(results[0].geometry.location);
        cgMap.setCenter(results[0].geometry.location);
        
        var cgmarker = new google.maps.Marker({
            map: cgMap, 
            position: results[0].geometry.location
        });
      } else {
        alert("Geocode was not successful for the following reason: " + status);
      }
    });
    grabTrails();
    grabSponsors();
}

function grabTrails(){
	//console.log('trails');
	$.getJSON('../services/trails.php',function(data){
		//console.log(data.trails);
		$.each(data.trails, function(i,item){
			//console.log(i);
             var statusClass = 'open';
             var status = 1;
             var where = "traillistBody";
		   	  
		   	  var dispLocation='';
		   	  if(item.trail.trailAddress == ""){dispLocation = item.trail.trailCity} else {dispLocation = item.trail.trailAddress}
		   	  buttonClass = "btn-primary";
		   	  buttonTxt = "Follow";
		   	 // if(item.trail.trailId == 22 || item.trail.trailId == 2){buttonClass = 'btn-success';buttonTxt = "Unfollow";}
			$('#'+where).append('<tr><td width="70"><a class="btn btn-success btn-mini" onclick="showTrailModal('+item.trail.trailId+')">Edit &raquo;</a></td><td> <a href="trail.php?t='+item.trail.trailId+'" class="'+statusClass+'">'+item.trail.trailName+'</a></td><td>'+dispLocation+'</td><td>'+item.trail.conditionDesc+'</td></tr>');
				//
			   		      
		 });
			
	});
	
}

var editor;

function replaceDiv( div )
{
	var config = {
		toolbar:
		[
			['Bold', 'Italic', '-', 'NumberedList', 'BulletedList', '-', 'Link', 'Unlink'],
			['UIColor']
		]
	};
	
	$(".jquery_ckeditor").ckeditor();
	
}

function saveTrail(){
	
}

function showTrailModal(trail){
	//alert(trail);
	var rr = new Date().getTime();
	$.getJSON('../services/conditions.php?'+rr,function(data){
		$("#conditions").empty();
		$.each(data.conditions, function(i,item){
			$("#conditions").append('<option value="'+item.condition.conditionId+'">'+item.condition.conditionDesc+'</option>');
		});
	});
	
	$.getJSON('../services/trails.php?t='+trail,function(data){
		//alert(data.trails[0].trail.trailName);
		$("#trailName").val("");
		$("#trailAddy").val("");
		$("#trailCity").val("");
		$("#landOwner").val("");
		$("#facebook").val("");
		$("#twitter").val("");
		$(".jquery_ckeditor").val("");
		$("#trailId").val("");
		var trail = data.trails[0].trail;
		//replaceDiv("trail_Desc");
		replaceDiv(  );
		$('#trailFrm').modal();
		$("#trailName").val(trail.trailName);
		$("#trailAddy").val(trail.trailAddress);
		$("#trailCity").val(trail.trailCity);
		$("#landOwner").val(trail.landOwner);
		$("#facebook").val(trail.facebook);
		$("#twitter").val(trail.twitter);
		$("#editor1").val(trail.trailDesc);
		$("#trailId").val(trail.trailId);
		
		$("#conditions").val(trail.currentCondition);
		$("#trailStatus").val(trail.currentStatus);
	});
	//$("#trailFrm").show();
}

function newTrail(){
	var rr = new Date().getTime();
	$.getJSON('../services/conditions.php?'+rr,function(data){
		$("#conditions").empty();
		$.each(data.conditions, function(i,item){
			$("#conditions").append('<option value="'+item.condition.conditionId+'">'+item.condition.conditionDesc+'</option>');
		});
	});
	$("#trailName").val("");
	$("#trailAddy").val("");
	$("#trailCity").val("");
	$("#landOwner").val("");
	$("#facebook").val("");
	$("#twitter").val("");
	$(".jquery_ckeditor").val("");
	$("#trailId").val("");
	replaceDiv(  );
	$('#trailFrm').modal();	
}

$(function(){
	grabTrails();
	
	 
		
	$("#saveTrail").click(function(){
		//console.log(editor.getData());
		
		$.post("../services/savetrail.php", $("#trail_frm").serialize(),
        	function(data){
	        	//console.log(data);
	        	if(data == 1){
		        	$("#onsave").append('Trail Saved');
		        	
	        	} else {
		        	$("#onsave").append('ERROR - Something went wrong, tell Clint');
		        	$("#onsave").addClass('alert-danger');
	        	}
	        	$("#onsave").show();
		  	}
		);

	});
	
})

//mapInit("Dallas, Texas");
