$(function(){
	$("#sign_in").click(function(){
		//alert("asdadadad");
	   doLogin();
   });
});

function logout(){
	//alert("erasing");
	eraseCookie("d_user");
	window.location = window.URL;
}

function doLogin(){
	
	$.post('login.php',$("#login_frm").serialize(),function(data){
		//console.log("!"+data+"!");
		if(data != "0"){
			//u = JSON.parse(data);
			createCookie("d_admin",data,2);
			window.location = 'main.php';
		} else {
			alert('Your login is incorrect, please try again');
		}
		
	});
}

//cookie functions
function createCookie(name,value,days) {
    if (days) {
        var date = new Date();
        date.setTime(date.getTime()+(days*24*60*60*1000));
        var expires = "; expires="+date.toGMTString();
    }
    else var expires = "";
    document.cookie = name+"="+value+expires+"; path=/";
}

function readCookie(name) {
    var nameEQ = name + "=";
    var ca = document.cookie.split(';');
    
    for(var i=0;i < ca.length;i++) {
        var c = ca[i];
        while (c.charAt(0)==' ') c = c.substring(1,c.length);
        //console.log(c);
        if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length,c.length);
    }
    return null;
}

function eraseCookie(name) {
    createCookie(name,"",-1);
}