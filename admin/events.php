<?php 
	include("../Connections/conn.php"); 
	
	$query_catRS = "CALL GetEvents(0);";
	$catRS = mysql_query($query_catRS, $conn) or die(mysql_error());
	$row_CatRS = mysql_fetch_assoc($catRS);
	$totalRows_channelRS = mysql_num_rows($catRS);
	
?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <title>DORBA Admin</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
    	<META HTTP-EQUIV="Pragma" CONTENT="no-cache">
	<META HTTP-EQUIV="Expires" CONTENT="-1">

     <link href="../assets/css/bootstrap.css" rel="stylesheet">
    <link type="text/css" href="../assets/css/ui-lightness/jquery-ui-1.8.23.custom.css" rel="stylesheet" />
    <style>
      body {
        padding-top: 60px; /* 60px to make the container go all the way to the bottom of the topbar */
      }
    </style>
    <link href="../assets/css/bootstrap-responsive.css" rel="stylesheet">

    <!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
      <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->

    <!-- Le fav and touch icons -->
    <link rel="shortcut icon" href="../assets/ico/favicon.ico">
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="../assets/ico/apple-touch-icon-144-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="../assets/ico/apple-touch-icon-114-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="../assets/ico/apple-touch-icon-72-precomposed.png">
    <link rel="apple-touch-icon-precomposed" href="../assets/ico/apple-touch-icon-57-precomposed.png">
  </head>

  <body>

    <div class="navbar navbar-inverse navbar-fixed-top">
      <div class="navbar-inner">
        <div class="container">
          <a class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse">
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </a>
          <a class="brand" href="#">DORBA Admin</a>
          <!--/.nav-collapse -->
        </div>
      </div>
    </div>

    <div class="container-fluid">
    	<div class="row">
	    	<div id="main_container" class="row-fluid">
	  		<div class="span2 well" id="sidebar">
		  		<ul class="nav nav-list bs-docs-sidenav">
			  		<li><a href="trails.php">Trails</a></li>
			  		<li><a href="sponsors.php">Sponsors</a></li>
			  		<li class="active"><a href="events.php">Events</a></li>
			  		<li><a href="members.php">Members</a></li>
			  	</ul>
			</div>
			<div class="span9" id="middle">
			<a id="addEvent" class="btn btn-success btn-mini">Add Event</a>
				<table class="table table-striped table-bordered table-condensed" id="eventList_tbl">
					<thead>
					  <tr>
					    <th></th>
					    <th>Name</th>
					    <th>Dates</th>
					    <th>On Home Page</th>
					  </tr>
					</thead>
					<tbody id="traillistBody">
						<?php do { ?> 
			              <tr><td width="140"><!-- <a href="#" class="btn btn-primary btn-mini" onclick="showImages(<?php echo $row_CatRS['id'];?>);"><i class="icon-picture icon-white"/></a> --> <a href="#" onclick="editEvent(<?php echo $row_CatRS['id'];?>);" class="btn btn-success btn-mini"><i class="icon-edit"></i></a>  <a class="btn btn-mini btn-danger" onclick="deleteEvent(<?php echo $row_CatRS['id']; ?>);"><i class="icon-remove-sign icon-white"></i></a></td><td><a href="event.php?e=<?php echo $row_CatRS['id']; ?>"><?php echo $row_CatRS['title']; ?></a></td><td><?php echo $row_CatRS['start']; ?></td><td><?php echo $row_CatRS['yesNo']; ?></td></tr>
			           <?php } while ($row_CatRS = mysql_fetch_assoc($catRS)); mysql_free_result($catRS); ?>
					</tbody>
				</table>
			</div>
			<div id="newEventFrm" style="display:none;">
		    	<div class="modal-header" id="hdr">New Event</div>
		    	<div>
		    		<form class="form-horizontal" id="newevent_frm">
		    			<input type="hidden" name="eventId" id="eventId" value="0"/>
					  <div class="control-group">
					    <label class="control-label" for="eventName">Event Name</label>
					    <div class="controls">
					      <input type="text" id="eventName" name="eventName" placeholder="">
					    </div>
					  </div>
					  <div class="control-group">
					    <label class="control-label" for="eventStart">Start Date</label>
					    <div class="controls">
					      <input type="text" id="eventStart" name="eventStart" placeholder="">
					    </div>
					  </div>
					  <div class="control-group">
					    <label class="control-label" for="eventEnd">End Date</label>
					    <div class="controls">
					      <input type="text" id="eventEnd" name="eventEnd" placeholder="">
					    </div>
					  </div>
					  <div class="control-group">
					    <label class="control-label" for="eventTime">Event Time</label>
					    <div class="controls">
					      <input type="text" id="eventTime" name="eventTime" placeholder="Ex 7:00am">
					    </div>
					  </div>
					  <div class="control-group">
					    <label class="control-label" for="eventLocation">Event Location</label>
					    <div class="controls">
					      <input type="text" id="eventLocation" name="eventLocation" placeholder="Enter Address" onblur="codeAddress()">
					    </div>
					    <div class="controls" id="geo">
					      <input type="text" id="geoLat" name="geoLat" class="input-small">/<input type="text" id="geoLang" name="geoLang" class="input-small">
					    </div>
					    <br/>
					    <label class="control-label" for="eventLocation2">- OR - Trail</label>
					    <div class="controls">
					      <select name="eventTrail" id="eventTrail"></select>
					    </div>
					  </div>
					  <div class="control-group">
					    <label class="control-label" for="shortDesc">Short Desc</label>
					    <div class="controls">
					      <textarea id="shortDesc" name="shortDesc" cols="40" rows="4" class="span3"></textarea>
					    </div>
					  </div>
					  <div class="control-group">
					    <label class="control-label" for="homepage">Show On Home Page</label>
					    <div class="controls">
					      <select id="homepage" name="homepage"><option value="0">No</option><option value="1">Yes</option></select>
					    </div>
					  </div>
					 <div class="control-group">
					    <textarea class="jquery_ckeditor" cols="80" id="editor1" name="editor1" rows="10"></textarea>
					  </div>
					</form>
		    	</div>
		    	<div class="modal-footer">
			    	<a href="#" id="saveEvent" class="btn btn-success">Save</a>
		    	</div>
		    </div>
	    </div>
      
    </div> <!-- /container -->
    <footer class="footer offset2"></footer>
    <div class="modal hide fade" id="eventfiles_modal">
    	<div class="modal-header">
    		Event Files<button type="button" class="close" aria-hidden="true" onclick="closeFiles()">×</button>
    	</div>
    	<div class="modal-body">
    	
    	</div>
    	<div class="modal-footer">
    		<a href="#" class="btn btn-success btn-mini pull-right">Done</a>
    	</div>
    </div>
    
    <!-- Le javascript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script type="text/javascript" src="../jquery/jquery-1.8.0.min.js"></script>
	<script type="text/javascript" src="../jquery/jquery-ui-1.8.23.custom.min.js"></script>
    <script type="text/javascript" src="ckeditor/ckeditor.js"></script>
    <script type="text/javascript" src="ckeditor/adapters/jquery.js"></script>
    <script src="../js/ajaxfileupload.js"></script>
    <script src="../assets/js/bootstrap-transition.js"></script>
    <script src="../assets/js/bootstrap-alert.js"></script>
    <script src="../assets/js/bootstrap-modal.js"></script>
    <script src="../assets/js/bootstrap-dropdown.js"></script>
    <script src="../assets/js/bootstrap-scrollspy.js"></script>
    <script src="../assets/js/bootstrap-tab.js"></script>
    <script src="../assets/js/bootstrap-tooltip.js"></script>
    <script src="../assets/js/bootstrap-popover.js"></script>
    <script src="../assets/js/bootstrap-button.js"></script>
    <script src="../assets/js/bootstrap-collapse.js"></script>
    <script src="../assets/js/bootstrap-carousel.js"></script>
    <script src="../assets/js/bootstrap-typeahead.js"></script>
    <!-- <script type="text/javascript" src="http://maps.googleapis.com/maps/api/js?key=AIzaSyBxbb7btwaq4ADW_y5cqQlM5GV2s3pyYB0&sensor=false"></script> -->
    <script src="../js/admin/events.js"></script>

  </body>
</html>
