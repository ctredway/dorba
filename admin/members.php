<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <title>DORBA Admin</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
	<META HTTP-EQUIV="Pragma" CONTENT="no-cache">
	<META HTTP-EQUIV="Expires" CONTENT="-1">

    <!-- Le styles -->
    <link href="../assets/css/bootstrap.css" rel="stylesheet">
    <style>
      body {
        padding-top: 60px; /* 60px to make the container go all the way to the bottom of the topbar */
      }
    </style>
    <link href="../assets/css/bootstrap-responsive.css" rel="stylesheet">

    <!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
      <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->

    <!-- Le fav and touch icons -->
    <link rel="shortcut icon" href="../assets/ico/favicon.ico">
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="../assets/ico/apple-touch-icon-144-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="../assets/ico/apple-touch-icon-114-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="../assets/ico/apple-touch-icon-72-precomposed.png">
    <link rel="apple-touch-icon-precomposed" href="../assets/ico/apple-touch-icon-57-precomposed.png">
  </head>

  <body>

    <div class="navbar navbar-inverse navbar-fixed-top">
      <div class="navbar-inner">
        <div class="container">
          <a class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse">
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </a>
          <a class="brand" href="#">DORBA Admin</a>
          <!--/.nav-collapse -->
        </div>
      </div>
    </div>

    <div class="container-fluid">
    	<div class="row">
	    <div id="main_container" class="row-fluid">
	  		<div class="span3 well">
		  		<ul class="nav nav-list bs-docs-sidenav">
			  		<li><a href="trails.php">Trails</a></li>
			  		<li><a href="sponsors.php">Sponsors</a></li>
			  		<li><a href="events.php">Events</a></li>
			  		<li class="active"><a href="members.php">Members</a></li>
			  	</ul>
			</div>
			<div class="span9">
				<div>
					<!--
<form class="form-inline"><label for="searchtxt">Search for Member: </label><input type="search" id="searchtxt" class="input-large search-query"> <a class="btn btn-success btn-small">Search</a> 
					<label class="checkbox">
						<input type="checkbox" id="memberOnly"> Show Members only
						</label> 
					</form>
-->
					
				</div>
				<table class="table table-bordered table-hover table-condensed table-striped" id="memberTbl">
	              <thead>
	                <tr>
	                  <th><a class="btn btn-primary btn-mini" id="addmem">Add</a></th>
	                  <th>Name</th>
	                  <th>User Name</th>
	                  <th>Email</th>
	                  <th>Membership Ends</th>
	                  <th></th>
	                </tr>
	              </thead>
	              <tbody id="memberList">
	              </tbody>
	            </table>
	            <?php include('newmember.php'); ?>
			</div>
      
	    </div>
      
    </div> <!-- /container -->
    <footer class="footer offset2"></footer>
    
    <!-- Le javascript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="../assets/js/jquery.js"></script>
    <script src="../assets/js/bootstrap-transition.js"></script>
    <script src="../assets/js/bootstrap-alert.js"></script>
    <script src="../assets/js/bootstrap-modal.js"></script>
    <script src="../assets/js/bootstrap-dropdown.js"></script>
    <script src="../assets/js/bootstrap-scrollspy.js"></script>
    <script src="../assets/js/bootstrap-tab.js"></script>
    <script src="../assets/js/bootstrap-tooltip.js"></script>
    <script src="../assets/js/bootstrap-popover.js"></script>
    <script src="../assets/js/bootstrap-button.js"></script>
    <script src="../assets/js/bootstrap-collapse.js"></script>
    <script src="../assets/js/bootstrap-carousel.js"></script>
    <script src="../assets/js/bootstrap-typeahead.js"></script>
    <script src="js/members.js"></script>
  </body>
</html>
