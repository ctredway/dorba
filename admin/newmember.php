<div id="new_member" class="well well-small span5" style="display:none">
	<a class="close pull-right" href="#" id="closeMemFrm">&times;</a>
	<form class="form-horizontal" id="member_frm">
		<div class="control-group">
			<label class="control-label" for="firstName">First Name:</label>
			<div class="controls">
				<input type="text" id="firstName" name="firstName" placeholder="First Name">
			</div>
		</div>
		<div class="control-group">
			<label class="control-label" for="lastName">Last Name:</label>
			<div class="controls">
				<input type="text" id="lastName" name="lastName" placeholder="Last Name">
			</div>
		</div>
		<div class="control-group" id="forum_cntrl">
			<label class="control-label" for="forumName">Forum Name:</label>
			<div class="controls">
				<input type="text" id="forumName" name="forumName" placeholder="Forum Name" onblur="checkUsername()">
			</div>
		</div>
		<div class="control-group">
			<label class="control-label" for="pwd">Password:</label>
			<div class="controls">
				<input type="password" id="pwd" name="pwd" placeholder="Password">
			</div>
		</div>
		<div class="control-group">
			<label class="control-label" for="email">Email:</label>
			<div class="controls">
				<input type="text" id="email" name="email" placeholder="Email">
			</div>
		</div>
		<div class="control-group">
			<label class="control-label" for="memberType">Member Type:</label>
			<div class="controls">
				<select id="memberType" name="memberType">
					<option value="0">-- Choose --</option>
					<option value="1">Individual</option>
					<option value="2">Family</option>
					<option value="3">Junior</option>
					<option value="4">Corporation</option>
				</select>
			</div>
		</div>
		<div class="control-group">
			<label class="control-label" for="homePhone">Home Phone:</label>
			<div class="controls">
				<input type="text" id="homePhone" name="homePhone" placeholder="Home Phone">
			</div>
		</div>
		<div class="control-group">
			<label class="control-label" for="cellPhone">Cell Phone:</label>
			<div class="controls">
				<input type="text" id="cellPhone" name="cellPhone" placeholder="Cell Phone">
			</div>
		</div>
		<div class="control-group">
			<label class="control-label" for="emerContact">Emergency Contact:</label>
			<div class="controls">
				<input type="text" id="emerContact" name="emerContact" placeholder="Emergency Contact">
			</div>
		</div>
		<div class="control-group">
			<div class="pull-left">
				<a id="cancelMember" class="btn btn-danger">Cancel</a>
				<a id="saveMember" class="btn btn-success">Save Member</a>
			</div>
		</div>
	</form>
</div>