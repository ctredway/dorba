 <?php 
	include("../Connections/conn.php"); 
	
	if(isset($_REQUEST['e'])){
		$query_catRS = "CALL GetEvents(".$_REQUEST['e'].");";
		$catRS = mysql_query($query_catRS, $conn) or die(mysql_error());
		$row_CatRS = mysql_fetch_assoc($catRS);
		$totalRows_channelRS = mysql_num_rows($catRS);
		if($totalRows_channelRS == 0){
			header( 'Location: events.php' ) ;
		}
	} else {
		header( 'Location: events.php' ) ;
	}	
	
?>

<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <title>DORBA Admin</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
    	<META HTTP-EQUIV="Pragma" CONTENT="no-cache">
	<META HTTP-EQUIV="Expires" CONTENT="-1">

     <link href="../assets/css/bootstrap.css" rel="stylesheet">
    <link type="text/css" href="../assets/css/ui-lightness/jquery-ui-1.8.23.custom.css" rel="stylesheet" />
    <style>
      body {
        padding-top: 60px; /* 60px to make the container go all the way to the bottom of the topbar */
      }
    </style>
    <link href="../assets/css/bootstrap-responsive.css" rel="stylesheet">

    <!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
      <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->

    <!-- Le fav and touch icons -->
    <link rel="shortcut icon" href="../assets/ico/favicon.ico">
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="../assets/ico/apple-touch-icon-144-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="../assets/ico/apple-touch-icon-114-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="../assets/ico/apple-touch-icon-72-precomposed.png">
    <link rel="apple-touch-icon-precomposed" href="../assets/ico/apple-touch-icon-57-precomposed.png">
  </head>

  <body>

    <div class="navbar navbar-inverse navbar-fixed-top">
      <div class="navbar-inner">
        <div class="container">
          <a class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse">
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </a>
          <a class="brand" href="#">DORBA Admin</a>
          <!--/.nav-collapse -->
        </div>
      </div>
    </div>

    <div class="container-fluid">
    	<div class="row">
	    	<div id="main_container" class="row-fluid">
	  		<div class="span2 well" id="sidebar">
		  		<ul class="nav nav-list bs-docs-sidenav">
			  		<li><a href="trails.php">Trails</a></li>
			  		<li><a href="sponsors.php">Sponsors</a></li>
			  		<li class="active"><a href="events.php">Events</a></li>
			  		<li><a href="members.php">Members</a></li>
			  	</ul>
			</div>
			<div class="span9" id="middle">

    <div class="container-fluid">
    
      <div class="row-fluid">
        
        <div class="span9">
        	<ul class="breadcrumb">
			  <li>
			    <a href="index2.php">Home</a> <span class="divider">/</span>
			  </li>
			  <li>
			    <a href="events.php">Events</a> <span class="divider">/</span>
			  </li>
			  <li class="active">
			    <?php echo $row_CatRS['eventName']; ?>
			  </li>
			 </ul>
        	<div class="page-header">
	        	<h2><?php echo $row_CatRS['eventName']; ?><!-- <a class="btn btn-primary btn-mini" style="float: right;">Follow Trail &raquo;</a> --></h2>
	        	
	        </div>
        	<div class="row-fluid">
	          <div class="well">
	            <?php echo $row_CatRS['eventDesc']; ?>
	          </div>
        	</div>
          <!--
<div class="row-fluid">
            <table class="table table-striped table-bordered table-condensed" id="trailList_tbl">
			<thead>
			  <tr>
			    <th></th>
			    <th>Name</th>
			    <th>Location</th>
			    <th>Condition</th>
			  </tr>
			</thead>
			<tbody id="listBody"></tbody>
		</table>
          </div>
--><!--/row-->
        </div><!--/span-->
      
        <div class="span3">
          <div class="well sidebar-nav">
          	<!--
<h4>Trail Map/Location</h4>
            <div id="smallMap"></div>
            <div><address><?php echo $row_CatRS['trailAddress']; ?></address></div>
            <h4>Trail Photos</h4>
            <div id="trailImgs"></div>
-->
          </div>
        </div><!--/span-->
      </div><!--/row-->

      <hr>

      <footer>
        <p>&copy; DORBA 2012</p>
      </footer>

    </div><!--/.fluid-container-->
    
    <!-- Le javascript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="../js/json2.js"></script>
    <script src="../assets/js/jquery.js"></script>
    <script src="../assets/js/bootstrap-transition.js"></script>
    <script src="../assets/js/bootstrap-alert.js"></script>
    <script src="../assets/js/bootstrap-modal.js"></script>
    <script src="../assets/js/bootstrap-dropdown.js"></script>
    <script src="../assets/js/bootstrap-scrollspy.js"></script>
    <script src="../assets/js/bootstrap-tab.js"></script>
    <script src="../assets/js/bootstrap-tooltip.js"></script>
    <script src="../assets/js/bootstrap-popover.js"></script>
    <script src="../assets/js/bootstrap-button.js"></script>
    <script src="../assets/js/bootstrap-collapse.js"></script>
    <script src="../assets/js/bootstrap-carousel.js"></script>
    <script src="../assets/js/bootstrap-typeahead.js"></script>
    <!-- <script src="js/app.js"></script> -->
     <script src="../js/login.js"></script> 
    <script>
    $(function(){
	   $("#home").removeClass("active");
	   $("#trails").removeClass("active"); 
	   $("#forums").removeClass("active");
	   $("#events").addClass("active");
	   $("#sponsors").removeClass("active");
	   
	   $("#sign_in").click(function(){
		   doLogin();
	   });
	   
	   if(readCookie("d_user") != null){
	   		u = readCookie("d_user");
	   		//console.log("WHAT THE: " + u);
		   //buildUserPnl(JSON.parse(u));
	   } else {
		   //console.log("asdasdsad");
	   }
    });
    
    </script>
  </body>
</html>
