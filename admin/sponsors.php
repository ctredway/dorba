<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <title>DORBA Admin</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
    	<META HTTP-EQUIV="Pragma" CONTENT="no-cache">
	<META HTTP-EQUIV="Expires" CONTENT="-1">

     <link href="../assets/css/bootstrap.css" rel="stylesheet">
    <link type="text/css" href="../assets/css/ui-lightness/jquery-ui-1.8.23.custom.css" rel="stylesheet" />
    <style>
      body {
        padding-top: 60px; /* 60px to make the container go all the way to the bottom of the topbar */
      }
    </style>
    <link href="../assets/css/bootstrap-responsive.css" rel="stylesheet">

    <!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
      <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->

    <!-- Le fav and touch icons -->
    <link rel="shortcut icon" href="../assets/ico/favicon.ico">
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="../assets/ico/apple-touch-icon-144-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="../assets/ico/apple-touch-icon-114-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="../assets/ico/apple-touch-icon-72-precomposed.png">
    <link rel="apple-touch-icon-precomposed" href="../assets/ico/apple-touch-icon-57-precomposed.png">
  </head>

  <body>

    <div class="navbar navbar-inverse navbar-fixed-top">
      <div class="navbar-inner">
        <div class="container">
          <a class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse">
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </a>
          <a class="brand" href="#">DORBA Admin</a>
          <!--/.nav-collapse -->
        </div>
      </div>
    </div>

    <div class="container-fluid">
    	<div class="row">
	    	<div id="main_container" class="row-fluid">
	  		<div class="span2 well" id="sidebar">
		  		<ul class="nav nav-list bs-docs-sidenav">
			  		<li><a href="trails.php">Trails</a></li>
			  		<li class="active"><a href="sponsors.php">Sponsors</a></li>
			  		<li><a href="events.php">Events</a></li>
			  		<li><a href="members.php">Members</a></li>
			  	</ul>
			</div>
			<div class="span10" id="middle">
			<a id="addSponsor" class="btn btn-success btn-mini">Add Sponsor</a>
				<table class="table table-striped table-bordered table-condensed" id="eventList_tbl">
					<thead>
					  <tr>
					    <th></th>
					    <th>Name</th>
					    <th>Website</th>
					    <!-- <th>Location</th> -->
					  </tr>
					</thead>
					<tbody id="sponsorlistBody">
						
					</tbody>
				</table>
			</div>
			<div class="span4">
			<div id="sponsorForm" style="display:none;">
		    	<div class="modal-header" id="hdr">New Sponsor
			    	
		    	</div>
		    	<div class="modal-body">
		    		<form class="form-horizontal" id="sponsor_frm">
		    			<input type="hidden" name="sid" id="sid" value="0"/>
					  <div class="control-group">
					    <label class="control-label" for="sponsorName">Sponsor Name</label>
					    <div class="controls">
					      <input type="text" id="sponsorName" name="sponsorName" placeholder="">
					    </div>
					  </div>
					  <div class="control-group">
					    <label class="control-label" for="website">Website</label>
					    <div class="controls">
					      <input type="text" id="website" name="website" placeholder="">
					    </div>
					  </div>
					  <div class="control-group">
					    <label class="control-label" for="contact">Contact</label>
					    <div class="controls">
					      <input type="text" id="contact" name="contact" placeholder="">
					    </div>
					  </div>
					 <div class="control-group">
					    <textarea class="jquery_ckeditor" cols="80" id="editor1" name="editor1" rows="10"></textarea>
					  </div>
					</form>
		    	</div>
		    	<div class="modal-footer">
			    	<a href="#" id="saveSponsorBtn" class="btn btn-success">Save</a>
		    	</div>
		
		    </div>
			</div>
	    </div>
	    <div id="locations" style="display:none;">LOCATIONS</div>
    </div> <!-- /container -->
    <footer class="footer offset1">Woot</footer>
    
    
    <!-- Le javascript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script type="text/javascript" src="../jquery/jquery-1.8.0.min.js"></script>
	<script type="text/javascript" src="../jquery/jquery-ui-1.8.23.custom.min.js"></script>
    <script type="text/javascript" src="ckeditor/ckeditor.js"></script>
    <script type="text/javascript" src="ckeditor/adapters/jquery.js"></script>
    <script src="../assets/js/bootstrap-transition.js"></script>
    <script src="../assets/js/bootstrap-alert.js"></script>
    <script src="../assets/js/bootstrap-modal.js"></script>
    <script src="../assets/js/bootstrap-dropdown.js"></script>
    <script src="../assets/js/bootstrap-scrollspy.js"></script>
    <script src="../assets/js/bootstrap-tab.js"></script>
    <script src="../assets/js/bootstrap-tooltip.js"></script>
    <script src="../assets/js/bootstrap-popover.js"></script>
    <script src="../assets/js/bootstrap-button.js"></script>
    <script src="../assets/js/bootstrap-collapse.js"></script>
    <script src="../assets/js/bootstrap-carousel.js"></script>
    <script src="../assets/js/bootstrap-typeahead.js"></script>
    <script src="../js/admin/sponsors.js"></script>

  </body>
</html>
