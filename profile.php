<?php include("header.php") ?>
<?php 
	$expired = false;
	if(isset($_REQUEST['u'])){
		date_default_timezone_set('America/Chicago');
	
		$memberQ = "Select u.user_id,firstName,lastName,u.user_email,DATE_FORMAT(membershipEnds,'%a, %b %e, %Y'),memberType,homePhone,cellPhone,emergencyContact,membershipEnds as endsOn,user_avatar from users u inner join memberData where user_id = ".$_REQUEST['u']." and member =".$_REQUEST['u'];
		$memberSQL = mysql_query($memberQ,$conn) or die(mysql_error());
		
		$member = mysql_fetch_row($memberSQL);
		$mType='';
		if($member[5] == 1){
			$mType='Individual Membership';
		} elseif($member[5] == 2){
			$mType='Family Membership';
		} elseif($member[5] == 3){
			$mType='Junior Membership';
		} elseif($member[5] == 4){
			$mType='Corporate Membership';
		}
		$today = date("Y-m-d");
		$cssClass='success';
		$showRenew = 'display:none';
		$expired=false;
		if($today > $member[9]){
			$expired = true;
			$cssClass = 'error';
			$showRenew = '';
		}
		
		$fname = $member[1];
		$lname = $member[2];
	}
	
	function user_avatar($avatar)
	{
	   $avatar_img = '';
	   $avatar_img = "forum/download/file.php?avatar=";
	   $avatar_img .= $avatar;
	   return $avatar_img;
	}
	//echo $member[1];
?>

<div class="container" id="main_content">
	<div class="row-fluid">
		<ul class="breadcrumb">
			  <li>
			    <a href="/">Home</a> <span class="divider">/</span>
			  </li>
			  <li class="active">
			    My Profile 
			  </li>
			  
			</ul>
		<div class="span10">
			<div id="memDisplay">
				<table class="table table-striped">
					<thead>
						<th colspan="2"><a class="btn btn-primary btn-small" href="#" id="showEdit_btn">Edit Profile</a></th>
					</thead>
					<tbody>
						<?php if($member[10] != ''){ ?> <tr><td>Avatar:</td><td><img src="<?php echo user_avatar($member[10]); ?>" class="img-rounded img-polaroid"></td></tr><?php } ?>
						<tr><td>Membership Type:</td><td><?php echo $mType; ?></td></tr>
						<tr class="<?php echo $cssClass; ?>"><td>Membership Ends:</td><td><?php echo $member[4]; ?> <a id="renew" href="#" style="<?php echo $showRenew; ?>" class="btn btn-success btn-small">Renew Now</a></td></tr>
						<tr><td>Name:</td><td><?php echo $member[1]." ".$member[2]; ?></td></tr>
						<tr><td>Email:</td><td><?php echo $member[3]; ?></td></tr>
						<tr><td>Home Phone:</td><td><?php echo $member[6]; ?></td></tr>
						<tr><td>Mobile:</td><td><?php echo $member[7]; ?></td></tr>
						<tr><td>Emergency Contact:</td><td><?php echo $member[8]; ?></td></tr>
					</tbody>
				</table>
			</div>
			<div id="editMember" style="display:none">
				<form class="form-horizontal" id="member_frm">
					<input type="hidden" name="u" value="<?php echo $_REQUEST['u']; ?>"/>
					<div class="control-group">
						<label class="control-label" for="firstName">First Name:</label>
						<div class="controls">
							<input type="text" id="firstName" name="firstName" placeholder="First Name" value="<?php echo $member[1]; ?>"/>
						</div>
					</div>
					<div class="control-group">
						<label class="control-label" for="lastName">Last Name:</label>
						<div class="controls">
							<input type="text" id="lastName" name="lastName" placeholder="Last Name" value="<?php echo $lname; ?>"/>
						</div>
					</div>
					<div class="control-group">
						<label class="control-label" for="homePhone">Home Phone:</label>
						<div class="controls">
							<input type="text" id="homePhone" name="homePhone" placeholder="Home Phone" value="<?php echo $member[6]; ?>"/>
						</div>
					</div>
					<div class="control-group">
						<label class="control-label" for="cellPhone">Mobile:</label>
						<div class="controls">
							<input type="text" id="cellPhone" name="cellPhone" placeholder="Cell Phone" value="<?php echo $member[7]; ?>"/>
						</div>
					</div>
					<div class="control-group">
						<label class="control-label" for="emerContact">Emergency Contact:</label>
						<div class="controls">
							<input type="text" id="emerContact" name="emerContact" placeholder="Emergency Contact" value="<?php echo $member[8]; ?>"/>
						</div>
					</div>
					
					<div class="control-group">
						<div class="pull-right">
							<a id="cancelMember1" class="btn btn-danger">Cancel</a>
							<a id="saveMember" class="btn btn-success">Save</a>
						</div>
					</div>
				</form>
			</div>	
			<?php include('choose_level2.php'); ?>
		</div>
	</div>  
</div>

<script src="js/json2.js"></script>
<script src="assets/js/jquery.js"></script>
<script src="assets/js/bootstrap-transition.js"></script>
<script src="assets/js/bootstrap-alert.js"></script>
<script src="assets/js/bootstrap-modal.js"></script>
<script src="assets/js/bootstrap-dropdown.js"></script>
<script src="assets/js/bootstrap-scrollspy.js"></script>
<script src="assets/js/bootstrap-tab.js"></script>
<script src="assets/js/bootstrap-tooltip.js"></script>
<script src="assets/js/bootstrap-popover.js"></script>
<script src="assets/js/bootstrap-button.js"></script>
<script src="assets/js/bootstrap-collapse.js"></script>
<script src="assets/js/bootstrap-carousel.js"></script>
<script src="assets/js/bootstrap-typeahead.js"></script>
<script src="js/login.js"></script>
<script src="js/profile.js"></script>
