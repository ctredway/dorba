<div id="new_member" class="well well-small span8" style="display:none">
	<a class="close pull-right" href="#" id="closeMemFrm">&times;</a>
	<form class="form-horizontal" id="member_frm">
		<div class="control-group">
			<label class="control-label" for="firstName">First Name:</label>
			<div class="controls">
				<input type="text" id="firstName" name="firstName" class="input-xxlarge" placeholder="First Name">
			</div>
		</div>
		<div class="control-group">
			<label class="control-label" for="lastName">Last Name:</label>
			<div class="controls">
				<input type="text" id="lastName" name="lastName" class="input-xxlarge" placeholder="Last Name">
			</div>
		</div>
		<div class="control-group" id="forum_cntrl">
			<label class="control-label" for="forumName">Forum Name:</label>
			<div class="controls">
				<input type="text" id="forumName" name="forumName" class="input-xxlarge" placeholder="Forum Name" onblur="checkUsername()">
			</div>
		</div>
		<div class="control-group">
			<label class="control-label" for="pwd">Password:</label>
			<div class="controls">
				<input type="password" id="pwd" name="pwd" class="input-xxlarge" placeholder="Password">
			</div>
		</div>
		<div class="control-group">
			<label class="control-label" for="email">Email:</label>
			<div class="controls">
				<input type="text" id="email" name="email" class="input-xxlarge" placeholder="Email">
			</div>
		</div>
		<div class="control-group">
			<label class="control-label" for="homePhone">Home Phone:</label>
			<div class="controls">
				<input type="text" id="homePhone" name="homePhone" class="input-xxlarge" placeholder="Home Phone">
			</div>
		</div>
		<div class="control-group">
			<label class="control-label" for="cellPhone">Cell Phone:</label>
			<div class="controls">
				<input type="text" id="cellPhone" name="cellPhone" class="input-xxlarge" placeholder="Cell Phone">
			</div>
		</div>
		<div class="control-group">
			<label class="control-label" for="emerContact">Emergency Contact:</label>
			<div class="controls">
				<input type="text" id="emerContact" name="emerContact" class="input-xxlarge" placeholder="Emergency Contact">
			</div>
		</div>
		<div class="control-group">
			<label class="control-label" for="terms">Liability Waiver:</label>
			<div class="controls">
				
				<textarea id="terms" cols="50" rows="15" disabled="true" class="input-xxlarge">
					To: Dallas Off-Road Bicycle Association (hereinafter collectively referred to as "DORBA"):
	 
					DEFINITIONS
					The term "trail maintenance" shall include all activities, services and use of facilities either provided by or arranged by DORBA, or any of the trails that DORBA chooses to hold an activity, including the one for which I am participating today, or in any way related to the design, construction or maintenance of a trail including, but not limited to: orientation and instructional session on how to use trail building and maintenance tools and implements which include but are not limited to [Insert every trail maintenance tool imaginable], transportation to and from sections of the trail, accommodations if and when necessary for sustained trail building and maintenance activities, loading and unloading of tools, equipment, and supplies necessary for trail building and maintenance in or around the recreation area.
					 
					The term "mountain biking" shall include all activities, services and use of facilities either provided by or arranged by DORBA, or any of the trails that DORBA chooses to hold an activity, including the one for which I am participating today, or in any way related to the riding of bicycles and any other vehicles (whether propelled by the rider thereof, self-propelled, or otherwise) including, but not limited to: orientation and instructional session, transportation, accommodation and recreational activities in addition to bicycling, loading, unloading and travel in or movement around the recreation area; and all other activities in the recreation area.
					 
					Assumption of Risks, Terrain, Wilderness Travel, Weather, Etc.
					I am aware that mountain biking (including participating in races sponsored and/or hosted by DORBA) involves risks, dangers and hazards in addition to those normally associated with bicycle riding. I acknowledge and accept that DORBA, their duly elected Board and their volunteers may fail to predict whether the terrain is safe for mountain biking. The terrain used for mountain biking is uncontrolled, may be unmarked, may not be inspected, and involves many risks, dangers and hazards. These may include, but are not limited to: ice and snow; trees, tree wells and tree stumps; creeks and running water; rocks; boulders; forest deadfall; holes and depressions; cliffs; variable conditions; crevasses; road banks, fences, and other man-made structures; impact or collision with other riders; the failure to bicycle safely or within one's own ability or within designated areas; negligence of other cyclists; and negligence on the part of DORBA, their duly elected Board and their volunteers, including the failure of DORBA, their duly elected Board and their volunteers to safeguard or protect me from the risks, dangers and hazards of mountain biking. Communication in the terrain where mountain biking takes place is difficult, and in the event of an accident, rescue and medical treatment may not be available.
					 
					I am aware that trail maintenance involves risks, dangers and hazards in addition to those normally associated with trail design, construction or maintenance. I acknowledge and accept that DORBA, their duly elected Board and their volunteers may fail to predict whether the terrain is safe for trail maintenance. The terrain used for trail maintenance is uncontrolled, may be unmarked, may not be inspected, and involves many risks, dangers and hazards. These may include, but are not limited to: ice and snow; trees, tree wells and tree stumps; exposure to wildlife generally found in the North Central Texas Area including but not limited to poisonous snakes and spiders, bobcats, deer, feral dogs, wild boar and jack rabbits; creeks and running water; rocks; boulders; forest deadfall; holes and depressions; cliffs; variable conditions; crevasses; road banks, fences, and other man-made structures; impact or collision with other riders; the failure to use maintenance tools and implements properly or within one's own ability or within designated areas; negligence of other persons participating in trail maintenance; and negligence on the part of DORBA, their duly elected Board and their volunteers, including the failure of DORBA, their duly elected Board and their volunteers to safeguard or protect me from the risks, dangers and hazards of trail maintenance. Communication in the terrain where trail maintenance takes place is difficult, and in the event of an accident, rescue and medical treatment may not be available.
					 
					I am aware, agree, and represent that I understand the nature of this activity, and that I am qualified, in good health, and in proper physical condition to participate in such an activity. I further agree and warrant that if at any time I believe conditions to be unsafe, or question my physical condition during the activity, I will immediately discontinue further participation in the activity.
					 
					I am aware of the above listed risks, dangers and hazards associated with trail maintenance and I freely accept and fully assume all such risks, dangers and hazards and the possibility of personal injury, death, property damage or loss resulting therefrom.
					 
					Release of Liability, Waiver of Claims and Indemnity Agreement
					In consideration of DORBA allowing me to participate in trail maintenance, and for other good and valuable consideration, the receipt and sufficiency of which is acknowledged, I hereby agree as follows:
					 
					1.       To waive any and all claims that I have or may have in the future against DORBA and their directors, officers, employees, volunteers, guides, agents, independent contractors, representatives, insurers, successors and assigns (all of whom are hereinafter collectively refereed to as the "Releasees") and to release the Releasees from any and all liability for any loss, damage, expense or injury, including death, that I may suffer or that my next of kin may suffer as a result of my participation in mountain biking, DUE TO ANY CAUSE WHATSOEVER, INCLUDING NEGLIGENCE (whether in whole or in part), BREACH OF CONTRACT, OR BREACH OF ANY STATUTORY OR OTHER DUTY OF CARE, ON THE PART OF THE RELEASEES, AND INCLUDING THE FAILURE ON THE PART OF THE RELEASEES TO SAFEGUARD OR PROTECT ME FROM THE RISKS, DANGERS AND HAZARDS OF MOUNTAIN BIKING REFERRED TO ABOVE;
					2.       To hold harmless and indemnify the Releasees from any and all liabilities, obligations, damages, penalties, claims, actions, costs, charges and expenses, including reasonable attorneys’ fees and other professional fees that may be imposed upon, incurred by or asserted against any of Releasees that arise out of or in connection with my participation in trail maintenance, including but not limited to property damage or personal injury to any third party;
					3.       That this Agreement shall be effective and binding upon my heirs, next of kin, executors, administrators, assigns and representatives, in the event of my death or incapacity;
					4.       This Agreement and any rights, duties and obligations as between the parties to this Agreement shall be governed by and interpreted solely in accordance with the laws of the State of Texas without regard to the conflicts of law principles thereof; and
					5.       Any litigation involving the parties to this Agreement shall be brought solely within the State of Texas and shall be within the exclusive jurisdiction of the Courts of the State of Texas.
					 
					In entering into this Agreement I am not relying on any oral or written representations or statements made by the Releasees with respect to the safety of mountain biking, other than what is set forth in this Agreement.
				</textarea>
			</div>
		</div>
		<div class="control-group">
			<div class="controls">
				<input type="checkbox" id="agree"/> I CONFIRM THAT I HAVE READ AND UNDERSTOOD THIS AGREEMENT PRIOR TO ACCEPTING IT, AND I AM AWARE THAT BY ACCEPTING THIS AGREEMENT I AM WAIVING CERTAIN LEGAL RIGHTS WHICH I OR MY HEIRS, NEXT OF KIN, EXECUTORS, ADMINISTRATORS, ASSIGNS AND REPRESENTATIVES MAY HAVE AGAINST THE RELEASEES. 
			</div>
		</div>
		<div class="control-group">
			<div class="pull-right">
				<a id="cancelMember" class="btn btn-danger">Cancel</a>
				<a id="saveMember" class="btn btn-success">Save Member</a>
			</div>
		</div>
	</form>
</div>

<!--
In consideration of membership in the Dallas Off Road Bicycle Association (hereinafter referred to as DORBA), I, for myself and my minor child/children, heirs, executors, administrators and assigns, hereby agree to forever release and discharge any and all rights, demands, claims and causes of suit or action, known or unknown, whether arising now or in the future, that I may have against DORBA, any other participating sponsors, its officers, directors, employees, representatives, agents and volunteers for any and all injuries, including death and any property damage in any manner arising or resulting from my participation or my child/children’s participation in any activity conducted by or in conjunction with DORBA.


I attest and verify that I have full knowledge of the risks involved in mountain bike riding and in all DORBA activities, that I assume those risks, that I will, without limitation, assume and pay any and all medical and emergency expenses incurred on my or my child/children’s behalf in the event of an accident, injury, illness, or other incapacity while participating in any DORBA activity, regardless of whether I have authorized such expenses. I further agree that in the event I require medical or surgical treatment while under the supervision of DORBA or any of its representatives, such DORBA representative may authorize medical treatment for myself.
-->