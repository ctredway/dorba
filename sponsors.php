    <?php include("header.php") ?>

    <div class="container-fluid">
    
      <div class="row-fluid">
        
        <div class="span9">
        	<div class="page-header">
	        	<h1>DORBA Sponsors</h1>
	        </div>
        	<div class="row-fluid">
        		<div class="well">
        		The following businesses show their support for DORBA and it's mission to provide great trails and great events by providing benefits to DORBA members.
<br/><br/>
For Sponsors, the program is simple and gives you access to an audience of over 4,000 local cyclists If you are interested in becoming a DORBA Trail Sponsor or otherwise supporting DORBA's mission of land access and trail building click <a href="contact.php">HERE</a>
 for more information and to apply to become DORBA sponsor.
        		</div>
	          <div class="well" id="spmap">
	            
	          </div>
        	</div>
          <div class="row-fluid">
                      </div><!--/row-->
        </div><!--/span-->
        
        <div class="span3">
          <table class="table table-striped table-bordered table-condensed" id="sponsorList_tbl">
			<thead>
			  <tr>
			    <th>Name</th>
			    <th>Location</th>
			  </tr>
			</thead>
			<tbody id="listBody"></tbody>
		</table>

        </div><!--/span-->
      </div><!--/row-->

      <hr>

      <footer>
        <p>&copy; DORBA 2012</p>
      </footer>

    </div><!--/.fluid-container-->
    
    <!-- Le javascript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="js/json2.js"></script>
    <script src="assets/js/jquery.js"></script>
    <script src="assets/js/bootstrap-transition.js"></script>
    <script src="assets/js/bootstrap-alert.js"></script>
    <script src="assets/js/bootstrap-modal.js"></script>
    <script src="assets/js/bootstrap-dropdown.js"></script>
    <script src="assets/js/bootstrap-scrollspy.js"></script>
    <script src="assets/js/bootstrap-tab.js"></script>
    <script src="assets/js/bootstrap-tooltip.js"></script>
    <script src="assets/js/bootstrap-popover.js"></script>
    <script src="assets/js/bootstrap-button.js"></script>
    <script src="assets/js/bootstrap-collapse.js"></script>
    <script src="assets/js/bootstrap-carousel.js"></script>
    <script src="assets/js/bootstrap-typeahead.js"></script>
    <script type="text/javascript" src="http://maps.googleapis.com/maps/api/js?key=AIzaSyBxbb7btwaq4ADW_y5cqQlM5GV2s3pyYB0&sensor=false&libraries=weather"></script>
    <!-- <script src="js/app.js"></script> -->
    <script src="js/login.js"></script>
    <script src="js/sponsors.js"></script>
    
    <script>
    $(function(){
	   $("#home").removeClass("active");
	   $("#trails").removeClass("active"); 
	   $("#forums").removeClass("active");
	   $("#events").removeClass("active");
	   $("#sponsors").addClass("active");
    });
    </script>
  </body>
</html>
