<div id="choose" class="well well-small span5" style="display:none">
<p>Please choose one of our membership options:</p>
<form id="pp_frm" class="form-inline" action="payments.php" method="post">
	<div class="control-group">
		<label class="control-label" for="memberType">Member Type:</label>
		<div class="controls">
			<select id="memberType" name="memberType" onchange="setPrice()">
				<option value="1">Individual - $26.03</option>
				<option value="2">Family - $31.17</option>
				<option value="3">Junior -  $10.59</option>
				<option value="4">Corporation - $77.48</option>
			</select>
		</div>
	</div>
	<div class="control-group">
		<div class="alert alert-info">
		All dues include a service fee
		</div>
	</div>
	<div class="control-group">
		<div class="pull-right">
			<a id="cancelMember" style="display:none" class="btn btn-danger">Cancel</a>
			<input type="submit" id="saveMember" class="btn btn-success" value="Pay">
		</div>
	</div>
	<input type="hidden" name="cmd" value="_xclick" /> 
    <input type="hidden" name="no_note" value="1" />
    <input type="hidden" name="lc" value="US" />
    <input type="hidden" name="currency_code" value="USD" />
    <input type="hidden" name="bn" value="PP-BuyNowBF:btn_buynow_LG.gif:NonHostedGuest" />
    <input type="hidden" name="first_name" value=""  />
    <input type="hidden" name="last_name" value=""  />
    <input type="hidden" name="payer_email" value=""  />
    <input type="hidden" name="item_number" value="99" / >
    <input type="hidden" name="memberCost" id="memberCost"/>
    <input type="hidden" name="memberDisplay" id="memberDisplay"/>
    <input type="hidden" name="memberId" id="memberId"/>
</form>
</div>